(defsystem "bop-afraid-bluebird"
;	:version "0.0.1"
	:description "\"afraid bluebird\" representation for entry IDs"
	:author "Valenoern"
	:licence "GPL"
	
	:depends-on ("cl-ppcre")
	:components (
		(:file "list-eff")  ; :bopwiki-bluebird-list-unedited
		(:file "bluebird" :depends-on ("list-eff"))  ; :bopwiki-bluebird
))
