;;; masto-preview.lisp - attempt to preview posts as on mastodon {{{
;;
;; I figured any non-programmer trying to use bop with mastodon would find it less of a nightmare to have this instead of having to look directly at ActivityStreams json
;; but do note:
;; bop activitypub is not ready for general use. it's best you wait until the manjaro/debian installer is updated to install "bop-rdf", so publishing your posts doesn't crash.

(defpackage :bop-activitypub-preview
	(:use :common-lisp :bopwiki :bop-render-core :bop-render-targets
		:bop-render-activitypub-actor  ; for exporting actor
	)
	(:local-nicknames
		(:bop :bopwiki) (:table :bopwiki)
		(:render :bop-render-core)
		(:buri :bop-render-uri)
		(:html :bop-render-html-copy) ; BUG: wrong package, should be moved
		(:ap :bop-render-activitypub)
		;; (:uiop :uiop)
	)
	(:export
		;; mastodon-preview-template export-mastodon-preview
		#:publish-preview-mastodon
))
(in-package :bop-activitypub-preview)
;;; }}}


(defun mastodon-preview-template ; html which will be exported {{{
	(&key entry-body)
	
	(format nil
"<!doctype html>
<html>
<head></head>
<body>

<main>
~a
</main>

</body></html>"
		entry-body)
) ; }}}

(defun export-mastodon-preview (actor pagename output) ; {{{
	(let (
		note-idno ;note-date url-list replying-to note-revision-url
		note-content
		(path (pathname-inside output "ap-preview"))
	)
	
	;; get entry information from entry table
	(let ((full-body (page-body pagename)) row)
		(setq
			row (bop-page-tablerow full-body)
			
			note-idno (bop-tablerow-idno row)  ; numerical ID
			;;note-date (epoch-to-iso (bop-tablerow-date row))  ; AP "published" string
			
			;;url-list (ap:ap-entry-links actor note-idno)  ; list of web links
			
			;;replying-to (ap:ap-entry-replyingto actor full-body)  ; inReplyTo
			
			;; page URI with revision number to play nicer with mastodon etc - see issues.bop/1631178631
			;;note-revision-url
			;;	(ap:ap-entry-id top-dir note-idno :revision (ap:page-ap-revision note-idno))
			
			;; get page excerpt
			note-content (ap:ap-entry-excerpt full-body :actor actor)
		)
	)
	
	;; export file {{{
	(ensure-directories-exist path)
	(export-in-dir  path (format nil "~d" note-idno)
		(mastodon-preview-template
			:entry-body note-content))
	;; }}}
	
	;; thingies {{{
	;; @context, generator - ignored for now
	;; attributedTo - show next to user handle
	;; to - show next to "Public" icon
	;; id - dunno where to put
	;; published - show next to date
	;; url - show below entry, + show as permalink
	;; content - important part. show as body of entry
	;; full json code - put below entry
	
	;; LATER:
	;; - show "=>" / extra "<=" links on entry
	;; - show avatar when that exists?
	;; - attend to issues listed on 'ap-note
	;; }}}
)) ; }}}


(defun publish-preview-mastodon (target cwd) ; {{{
"Create friendly preview of what ActivityStreams entries might look like on mastodon.
Can be used as a publish format in 'BOP-RENDER:REGISTER-PUBLISH-TARGET"
	(let (output actor
		;; publish formats take a standardised 'publish target' struct,
		;; to better enable updates to 'bop publish' without updating individual formats
		(page-list (publish-target-pages target))
	)
	cwd  ; unused required argument, leave alone
	
	;; if no activitypub publish path, use the bop-render one
	(when (null ap:*publish-activitypub-path*)
		(ap:publish-activitypub-path render:*default-publish-path*))
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname ap:*publish-activitypub-path*))
	(ensure-directories-exist output)
	
	;; get actor information
	(setq actor (ap-actor-info))
	
	;; render individual non-revision pages
	(dolist (pagename page-list)
		(export-mastodon-preview actor pagename output))
	
	;; BUG: actor entry has to be in page list to correctly subset pages
	
	;; LATER: move some repeated stuff in publish functions to render core
)) ; }}}
