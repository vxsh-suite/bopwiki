;;; ap-actor.lisp - ap actor.json file {{{
(defpackage :bop-render-activitypub-actor
	(:use :common-lisp :bopwiki
		:bop-render-uri
		:bop-render-author  ; experimental bop-rdf duct tape
	)
	(:local-nicknames
		;; (:uiop :uiop)
		(:render :bop-render-core)
		(:buri :bop-render-uri)
		(:author :bop-render-author)
	)
	(:export
		#:ap-actor-info #:ap-actor
		
		#:ap-user #:ap-user-basic #:ap-user-pubkey #:ap-user-title #:ap-user-p #:make-ap-user
		#:ap-user-handle #:ap-user-host
		;; ap-user-page
		#:ap-user-topdir #:ap-user-id #:ap-user-inbox #:ap-user-outbox #:ap-user-entries
))
(in-package :bop-render-activitypub-actor)
;;; }}}


(defstruct ap-user
;; bop-render AP version of WEB-AUTHOR
	basic  ; a WEB-AUTHOR struct
	pubkey
	title
)


(defun ap-actor-info () ; get actor info using :bop-render-author {{{
	;; try to read stem author info from page table
	(let ((actor-info (author:test))
		(pubkey "-----BEGIN PUBLIC KEY-----...example...-----END PUBLIC KEY-----")
		actor-handle actor-host actor-title pubkey-path)
	
	(setq actor-handle
		(gethash "http://distributary.network/owl-wing/2021-06/bop#username" actor-info))
	(setq actor-host
		(gethash "http://distributary.network/owl-wing/2021-06/bop#host" actor-info))
	(setq actor-title
		(gethash "http://distributary.network/owl-wing/2021-06/bop#title" actor-info))
	
	;; try to load public key from file
	;; LATER: once signing with private key is implemented, move loading pubkey elsewhere
	(setq pubkey-path
		(gethash "http://distributary.network/owl-wing/2021-06/bop#keyPathPublic" actor-info))
	(unless (null pubkey-path)
		;; simply using read-file-string won't flatten the key into one line
		(setq pubkey (join-str-with (uiop:read-file-lines pubkey-path) "")))
	
	(make-ap-user
		:basic
			(make-web-author
				:handle actor-handle
				:host actor-host
				:path "/bop/~a/")
					;; LATER: allow configuring a different url format
		:pubkey pubkey
		:title actor-title
	)
)) ; }}}

;; LATER: un-bork after move
(defun ap-actor (actor-info output) ; json actor object {{{
	(let ((path output) actor
		(username (ap-user-handle actor-info)) (display-name (ap-user-title actor-info))
		(actor-id (ap-user-id actor-info))
		(inbox (ap-user-inbox actor-info)) (outbox (ap-user-outbox actor-info))
		(public-key (ap-user-pubkey actor-info))
	)
	
	;; assemble actor object {{{
	(let (pubkey-box
		(context "\"@context\": [ \"https://www.w3.org/ns/activitystreams\", \"https://w3id.org/security/v1\" ],"))
		
		;; information about actor's public key
		(setq pubkey-box (format nil
			"\"publicKey\": { \"id\": \"~a#main-key\", \"owner\": \"~a\", \"publicKeyPem\": \"~a\" }"
			actor-id actor-id public-key
		))
		
		;; main information about actor
		(setq actor (format nil
"{ ~a  \"type\": \"Person\", \"id\": \"~a\", \"preferredUsername\": \"~a\", \"name\": \"~a\", \"inbox\": \"~a\", \"outbox\": \"~a\",
~a }"
			context
			actor-id username display-name inbox outbox
			pubkey-box
			;; LATER: see if name is correct for display name
		))
	)
	;; }}}
	
	;; LATER:
	;; provide 'index.htm' as profile link so mastodon doesn't recommend actor.json for more posts
	
	;; export to actor.json
	(render:export-in-dir path (make-pathname :name "actor" :type nil) actor)
)) ; }}}


;; "accessors" for activitypub author ; {{{

(defun ap-user-handle (actor)
	(web-author-handle (ap-user-basic actor)))

(defun ap-user-host (actor)
	(web-author-host (ap-user-basic actor)))

;; any function that needs "path" should be already be complex enough it's not a problem to retrieve :basic first

(defun ap-user-page (actor page  &key raw) ; {{{
;; get the url of an activitypub API page
;; example: https://example.com/bop/user/inbox
	(let ((author (ap-user-basic actor)) web-link)
	
	(setq web-link
		(construct-web-link
			:scheme 'https
			:host (web-author-host author)
			:path
				(format nil "~a~a"
					(format nil (web-author-path author)  ; example: "/bop/~a/"
						(web-author-handle author))
					page)))
	
	(if (null raw)
		(buri:render-web-link web-link)
		web-link)
)) ; }}}

(defun ap-user-topdir (author  &key raw)  ; top directory for published bop entries
;; example: https://example.com/bop/user/
	(ap-user-page author "" :raw raw))

(defun ap-user-id (author)  ; ActivityStreams actor object
;; example: https://example.com/bop/user/actor
	(ap-user-page author "actor"))

(defun ap-user-inbox (author)  ; nonfunctional dummy inbox
;; example: https://example.com/bop/user/inbox
	(ap-user-page author "inbox"))

(defun ap-user-outbox (author)  ; list of posts
;; example: https://example.com/bop/user/outbox
	(ap-user-page author "outbox"))

(defun ap-user-entries (author  &key raw)
	(ap-user-page author "ap-entry/" :raw raw)
)

;; it's probably best these inner paths are the same for all bop blogs,
;; so interactions with them from other things are especially consistent.
;; however it is not totally guaranteed they will stay the same forever or never become configurable per "bop host".
;; }}}
