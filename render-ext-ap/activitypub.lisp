;;; activitypub.lisp - output fediverse sharing information {{{
(defpackage :bop-render-activitypub
	(:use :common-lisp
		:bopwiki
			;; epoch-to-iso
		:bop-render-core :bop-render-targets
		:bop-render-activitypub-actor  ; for exporting actor
	)
	(:local-nicknames
		;; (:osicat :osicat) (:uiop :uiop)
		(:bop :bopwiki) (:table :bopwiki)
		(:render :bop-render-core)
		(:buri :bop-render-uri)
		(:html :bop-render-html-copy) ; BUG: wrong package, should be moved
		(:actor :bop-render-activitypub-actor)
		(:json :bop-render-ap-json)
	)
	(:export
		;; config
		#:*publish-activitypub-path* #:publish-activitypub-path
		
		#:ap-entry-excerpt
		; ap-note
		#:publish-bop-activitypub
		
		;; not really supposed to be exported but have to for preview
		#:ap-entry-html-link #:ap-entry-links
		#:ap-entry-replyingto
		#:page-ap-revision #:ap-entry-id
))
(in-package :bop-render-activitypub)
;;; }}}


;;; config functions {{{

(defparameter *publish-activitypub-path* nil) ; {{

(defun publish-activitypub-path (path)
"Set directory to publish rendered entries to when running 'bop publish', using the lisp pathname PATH.
This function is designed to be used in init.lisp, and to nest any arbitrary function to calculate the pathname, ex. (namestring \"/path/to/output\")"
	(let ((path-object (uiop:ensure-pathname path)))
	(setq *publish-activitypub-path* path-object)
))
;; }}

;;; }}}


;; Link objects pointing to entry {{{

(defun ap-entry-plain-link (actor numeric-id) ; Link object for un-revision'd entry {{{
	(buri:construct-web-link
		;;:rel "canonical"
		:mimetype "application/activity+json"
		:scheme 'https  ; in activitypub everything should be https:
		:host (ap-user-host actor)
		:path  ; get lisp entries path
			(buri:web-link-path (ap-user-entries actor :raw t))
		:name (format nil "~d" numeric-id) :type nil)
	
	;; a rel=canonical link actually shows up as the 'permalink' in mastodon, which is cool
	;;   although this isn't the canonical link for an /html user interface/
	;;   LATER: figure out the correct thing to do there.
) ; }}}

(defun ap-entry-html-link (actor page-id) ; Link object for html page {{{
	;; LATER:
	;; this should be in html system, gemini's in gmi system, etc, and then "registered" 
	(let (
		(pagename (table:page-basename page-id))
	)
	(buri:construct-web-link
		:mimetype "text/html"
		:scheme 'https
		:host (ap-user-host actor)
		:path (ap-user-topdir actor :raw t) ; example: "/bop/examplename/"
		:name pagename :type "htm")
)) ; }}}

(defun ap-entry-links (actor pagename) ; reference to entry's webpage(s) {{{
;; used for ap-note and outbox file
	(let (url-list)
	
	;; in theory you can offer multiple web uris in multiple formats
	;; LATER: do this more dynamically based on what's available, like adding .gmi if it exists
	(setq url-list (list
		(ap-entry-plain-link actor pagename)
		(ap-entry-html-link actor pagename)
	))
	
	url-list
)) ; }}}

;; }}}

;; revision numbers {{{

(defun ap-entry-revision-slug (entry-id revision) ; {{{
;; this function name may be a bit long for what it does
	(when (stringp entry-id)
		(setq entry-id (parse-integer entry-id))  ; remove zerofills
	)
	(format nil "~a_r~a" entry-id revision)
) ; }}}

(defun ap-entry-id (actor entry-id  &key revision) ; uri identifying entry {{{
	(let (
		(entry-dir (ap-user-entries actor))
		(entry-slug
			;; for now assume revision will be either nil or a valid number
			(if (null revision)
				(format nil "~a" entry-id)
				(ap-entry-revision-slug entry-id revision))
	))
	(format nil "~a~a"  entry-dir  entry-slug)
)) ; }}}

;; LATER: this is only temporary, and planned to be replaced later
(defun load-ducttape-meta (page-id meta-name) ; {{{
;; currently there is no good way to store arbitrary entry metadata particularly if it's extraneous
;; so I'm dumping this in each stem's /.bop/ folder and opening those.
;; I would like to replace this duct tape with some kind of real rdf metadata system, but it may take a while to arrive.
	(let ((top-dir bopwiki:*top-directory*) bop-dir filename meta-value)
	
	(setq bop-dir (pathname-inside top-dir ".bop"))
	(when (and (uiop:directory-exists-p bop-dir) (not-null meta-name))
		(setq filename
			(merge-pathnames bop-dir (make-pathname
				:name (format nil "meta-shim--~a--~a" page-id meta-name) :type nil))
		)
		(when (uiop:file-exists-p filename)
			(setq
				meta-value (uiop:read-file-lines filename)
				meta-value (nth 0 meta-value)  ; take first line and leave any possible newlines
		))
	
		;; return meta-value if present, or nil
		;; if no .bop directory, also return nil
		(if (not-null meta-value)  meta-value nil)
))) ; }}}

(defun page-ap-revision (entry-id) ; 'accessor' for meta {{{
	(let ((meta-file (load-ducttape-meta entry-id "revision")) revision-str)
	(setq revision-str
		(if (null meta-file)
			"1" meta-file))
	(parse-integer revision-str)
)) ; }}}

;; }}}

(defun ap-entry-replyingto (actor full-body) ; {{{
	(let ((meta-lines (bop-page-meta full-body)) parent-links)
	
	(dolist (meta meta-lines)
		(let ((kind (meta-line-kind meta)) (value (meta-line-value meta))
			revision-uri)
		
		(when (and
			(equal kind "cxt") (table:page-exists-p value)
			)
			(setq
				revision-uri (ap-entry-id actor value :revision (page-ap-revision value))
				parent-links (cons revision-uri parent-links)
		))
		;; LATER: when meta line revamp is finished,
		;; :kind will probably contain the meta line data function
		;;   that returns all the characteristics of the meta line.
		;; at that time, un-hardcode "cxt"
	))
	
	(reverse parent-links)
)) ; }}}

(defun ap-entry-excerpt (full-body  &key actor) ; {{{
	(let ((note-idno (bop-tablerow-idno (bop-page-tablerow full-body))) (IS-EXCERPT t))
	
	;; get page excerpt if missing
	(when (null (bop-page-excerpt full-body))
		(setf (bop-page-excerpt full-body) (render:excerpt-text full-body))
	)
	;; render excerpt
	(html:format-body-html  full-body IS-EXCERPT :newline ""
		:meta-links t
		:author (ap-user-basic actor)
		:absolute-url (buri:render-web-link (ap-entry-html-link actor note-idno))
		;; LATER: now that meta-links exists, clean this argument up
	)
)) ; }}}

(defun export-entry-synonymised (path note-body  &key idno revision actor) ; {{{
;; export revision-numbered files
	(let (safe-id canonical-basename relative-canonical-path link-path) ;canonical-path
	(setq
		safe-id (format nil "~a" idno)
		canonical-basename (make-pathname :name safe-id :type nil)
		
		relative-canonical-path
			(make-pathname :directory '(:relative ".") :name safe-id :type nil)
		link-path (merge-pathnames path (ap-entry-revision-slug idno revision))
	)
	
	;; export main file
	(export-in-dir  path canonical-basename  note-body)
	
	;; symlink revision-numbered uri to plain one
	(unless (uiop:file-exists-p link-path)
		(osicat:make-link link-path :target relative-canonical-path)
	)
	;; BUG: haven't yet tested bop on an OS that doesn't support symlinks
	;; if so, re-export the file as they're not too huge
	;(export-in-dir  path (make-pathname :name basename :type nil)  note-body)
	
	;; fill in missing revision numbers with Tombstones
	(when (> revision 1) ; {{{
		(let (
			(minimum 1) (maximum (- revision 1)) dead-revision full-path tombstone
			)
			(loop for i  from minimum to maximum  do
				(setq
					dead-revision (merge-pathnames path (ap-entry-revision-slug idno i))
					full-path (merge-pathnames path dead-revision)
					tombstone
						(json:ap-tombstone-json :former-type "Note"
							:id (ap-entry-id actor idno :revision i))
				)
				;; remove any symlinks to avoid overwriting main entry
				(uiop:delete-file-if-exists full-path)
				(export-in-dir  path dead-revision  tombstone)
			))) ; }}}
	
)) ; }}}

(defun ap-note (actor pagename output) ; individual AP entry objects {{{
	(let (note note-idno note-date note-content replying-to ;note-id
		(path (pathname-inside output "ap-entry"))
		url-list ap-revision note-revision-id
	)
	
	;; get entry information from entry table
	(let ((full-body (page-body pagename)) row) ; {{{
		(setq
			row (bop-page-tablerow full-body)
			
			note-idno (bop-tablerow-idno row)  ; numerical ID
			note-date (epoch-to-iso (bop-tablerow-date row))  ; AP "published" string
			
			url-list (ap-entry-links actor note-idno)  ; list of web links
			
			replying-to (ap-entry-replyingto actor full-body)  ; inReplyTo
			
			;; page URI with revision number to play nicer with mastodon etc - see issues.bop/1631178631
			ap-revision (page-ap-revision note-idno)
			note-revision-id
				(ap-entry-id actor note-idno :revision ap-revision)
			
			;; get page excerpt
			note-content (ap-entry-excerpt full-body :actor actor)
	)) ; }}}
	
	;; construct activitypub json
	(setq note ; {{{
		(json:ap-message-json
			:actor (ap-user-id actor)
			:type "Note"  :id note-revision-id  :published note-date
			:url (json:ap-url-json url-list)
			:content note-content
			:replying-to replying-to
			
			;; entries are all classified as Notes because of a mastodon related problem - see issues.bop/1631178631
	)) ; }}}
	
	;; LATER: {{{
	;; - implement "updated" field using file modified time to better fit the standard for editable platforms
	;; - use h1 as entry "name"
	;; - include meta line info in "tag"
	;; - include "attachment", + "preview" thumbnail when important
	;; - consider setting content warning flag when applicable - issues.bop/1631256741
	;; }}}
	
	(ensure-directories-exist path)
	(export-entry-synonymised  path note :idno note-idno :revision ap-revision :actor actor)
)) ; }}}

(defun ap-outbox (actor pagename-list output-dir) ; create outbox collection + dummy inbox {{{
	(let ((pages-sorted (sort-pages-ascending pagename-list)) collection outbox)
	
	(dolist (pagename pages-sorted)
		(let ((page-id (table:page-id pagename)) revision entry-uri)
		;; LATER: having to load this file again is overhead. put in page table at some point
		(setq revision (page-ap-revision page-id))
		
		(setq entry-uri (format nil "\"~a\"" (ap-entry-id actor page-id :revision revision)))
		(setq collection (cons entry-uri collection))
		;; pages are added to collection in reverse chrono order, but that's okay
	))
	
	(setq outbox (json:ap-outbox-json collection))
	
	(export-in-dir output-dir (make-pathname :name "outbox" :type nil)  outbox)
	(export-in-dir output-dir (make-pathname :name "inbox" :type nil)  "")  ; intentionally empty
)) ; }}}

(defun publish-bop-activitypub (target cwd) "publish wiki folder to AP" ; {{{
	(let (output actor page-list)
	cwd  ; unused required argument, leave alone
	
	;; if no publish path, use the bop-render one
	(when (null *publish-activitypub-path*)
		(publish-activitypub-path render:*default-publish-path*))
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname *publish-activitypub-path*))
	(ensure-directories-exist output)
	
	;; get actor information, then export
	(setq actor (actor:ap-actor-info))
	(actor:ap-actor actor output)
	
	;; publish formats now take a standardised 'publish target' struct,
	;; to better enable updates to how 'bop publish' works without updating individual formats
	(setq page-list (publish-target-pages target))
	
	;; render individual non-revision pages
	(dolist (pagename page-list)
		(ap-note actor pagename output))
	;; export list of published pages + dummy inbox
	(ap-outbox actor page-list output)
	
	;; BUG: actor entry has to be in page list to correctly subset pages
)) ; }}}


;; LATER: complete remaining tasks to make this a "beta quality demo" - issues.bop/1628402237
;;
;; AP docs - https://blog.joinmastodon.org/2018/06/how-to-implement-a-basic-activitypub-server/
;; webfinger - https://datatracker.ietf.org/doc/html/rfc7033
;; AS object vocab - https://www.w3.org/TR/activitystreams-vocabulary/

;; documentation checklist
;; - what on earth is webfinger / references
;; - (required tasks unknown)
