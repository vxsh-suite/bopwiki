(defsystem "bop-render-activitypub"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "activitypub syndication (read-only / just outbox)"
	
	:depends-on (
		"uiop" "osicat" "puri"
		"bopwiki" "bop-render"
		"bop-render-html"  ; for rendering post bodies in html
	)
	:components (
		(:file "ap-actor")
		(:file "ap-webfinger")  ; signpost to activitypub content
		
		(:file "ap-json")  ; :bop-render-ap-json - output json objects
		(:file "activitypub"  ; :bop-render-activitypub - activitypub content
			:depends-on ("ap-actor" "ap-json"))
		
		(:file "masto-preview" :depends-on ("activitypub"))  ; friendly mastodon post previewer
))

;; note: this is extremely experimental.
;; the 'bop rdf' system(s) used to get actor data are rather unfinished and this version of the program might not work out of the box in the future.
;; hopefully 'bop-rdf' will be more complete and stable by later versions of bop and this notice will be a distant memory.
