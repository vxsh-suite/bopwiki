;;; tests for body formatting rules {{{
(defpackage :bopwiki-test-formatting
	(:use :common-lisp :bopwiki :fiveam :bopwiki-test-helpers
		:bop-render
	
		:bop-render-html-2
		:bop-render-html-copy
	)
	(:local-nicknames
		(:rules :bopwiki-linerules)
	)
	(:export
		;; #:formatting-test-h1 #:formatting-test-h2 #:formatting-test-uniquote #:formatting-test-mdpre #:formatting-test-unihr
		;;#:formatting-test-mdli #:formatting-test-mdlicheck
		
		;;#:formatting-test-mdpre-complex
		
		#:run-tests
))
(in-package :bopwiki-test-formatting)

(def-suite formatting-tests)  ; fiveam
(in-suite formatting-tests)
;;; }}}


;;; simple {{{
;; note: 'h1' is not enabled on most of these so '# entry 1.01' is not an error

(defun formatting-test-h1 () ; {{{
	(let (
		(entry  (format-body-html  
		(example-page-file
			:id 946986926  :title "formatting"  :body
			'("# formatting 1.04" "" "the above should render as a heading")
			:bodyrules '("h1")
		) nil))
	)
	
	#| reference:
<h1>formatting 1.04</h1>
<br />
the above should render as a heading<br />
	|#
	(string-equals-bytes  entry (vector ; {{{
		60 104 49 62 102 111 114 109 97 116 116 105 110 103 32 49 46 48 52 60 47 104 49 62 10 60 98 114 32 47 62 10 116 104 101 32 97 98 111 118 101 32 115 104 111 117 108 100 32 114 101 110 100 101 114 32 97 115 32 97 32 104 101 97 100 105 110 103 60 98 114 32 47 62 10
	)) ;; }}}
)) ; }}}

(defun formatting-test-h2 () ; {{{
	(let (
		(entry  (format-body-html  
		(example-page-file
			:id 946986926  :title "formatting"  :body
			'("# formatting 1.04" "" "## second heading" "the above should render as a heading")
			:bodyrules '("md-h2")
		) nil))
	)
	
	#| reference:
# formatting 1.04<br />
<br />
<h2>second heading</h2>
the above should render as a heading<br />
	|#
	(string-equals-bytes  entry (vector ; {{{
		35 32 102 111 114 109 97 116 116 105 110 103 32 49 46 48 52 60 98 114 32 47 62 10 60 98 114 32 47 62 10 60 104 50 62 115 101 99 111 110 100 32 104 101 97 100 105 110 103 60 47 104 50 62 10 116 104 101 32 97 98 111 118 101 32 115 104 111 117 108 100 32 114 101 110 100 101 114 32 97 115 32 97 32 104 101 97 100 105 110 103 60 98 114 32 47 62 10
	)) ;; }}}
)) ; }}}

(defun formatting-test-uniquote () ; {{{
	(let (
		(entry  (format-body-html  
		(example-page-file
			:id 1609754871  :title "quote"  :body
			'("❝" "Long quote" "several lines long" "❞"
			"the quote should be put inside a blockquote")
			:bodyrules '("uni-quote")
		) nil))  ; :one-line t
	)
	
	#| reference:
# quote 1.04<br />
<br />
<blockquote>
Long quote<br />
several lines long<br />
</blockquote>
the quote should be put inside a blockquote<br />
	|#
	(string-equals-bytes  entry (vector ; {{{
		35 32 113 117 111 116 101 32 49 46 48 52 60 98 114 32 47 62 10 60 98 114 32 47 62 10 60 98 108 111 99 107 113 117 111 116 101 62 10 76 111 110 103 32 113 117 111 116 101 60 98 114 32 47 62 10 115 101 118 101 114 97 108 32 108 105 110 101 115 32 108 111 110 103 60 98 114 32 47 62 10 60 47 98 108 111 99 107 113 117 111 116 101 62 10 116 104 101 32 113 117 111 116 101 32 115 104 111 117 108 100 32 98 101 32 112 117 116 32 105 110 115 105 100 101 32 97 32 98 108 111 99 107 113 117 111 116 101 60 98 114 32 47 62 10
	)) ;; }}}
)) ; }}}

(defun formatting-test-mdpre () ; {{{
	(let (
		(entry  (format-body-html  
		(example-page-file
			:id 1609754872  :title "pre"  :body
			'("```" "╔═══════════════╗" "║   pre block   ║" "╚═══════════════╝" "```"
			"this should be put inside a preformatted area")
			:bodyrules '("md-pre")  ; md-pre
		) nil))  ; :one-line t
	)
	
	#| reference:
# pre 1.04<br />
<br />
<pre>╔═══════════════╗
║   pre block   ║
╚═══════════════╝
</pre>
this should be put inside a preformatted area<br />
	|#
	(string-equals-bytes  entry (vector ; {{{
		35 32 112 114 101 32 49 46 48 52 60 98 114 32 47 62 10 60 98 114 32 47 62 10 60 112 114 101 62 226 149 148 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 151 10 226 149 145 32 32 32 112 114 101 32 98 108 111 99 107 32 32 32 226 149 145 10 226 149 154 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 144 226 149 157 10 60 47 112 114 101 62 10 116 104 105 115 32 115 104 111 117 108 100 32 98 101 32 112 117 116 32 105 110 115 105 100 101 32 97 32 112 114 101 102 111 114 109 97 116 116 101 100 32 97 114 101 97 60 98 114 32 47 62 10
	)) ;; }}}
)) ; }}}

(defun formatting-test-unihr () ; {{{
	(let (
		(entry  (format-body-html  
		(example-page-file
			:id 1609755188  :title "divider"  :body
			'("# dividers" "" "─" "" "this dash will render as a 'hr' tag." "" "-" ""
			"this hyphen should not" "─ nor should this line starting with one")
			:bodyrules '("uni-hr")
		) nil))  ; :one-line t
	)
	
	#| reference:
# dividers<br />
<br />
<hr />
<br />
this dash will render as a 'hr' tag.<br />
<br />
-<br />
<br />
this hyphen should not<br />
─ nor should this line starting with one<br />
	|# ; BUG: wrong. header probably shouldn't be duplicated.
	(string-equals-bytes  entry (vector ; {{{
		35 32 100 105 118 105 100 101 114 115 60 98 114 32 47 62 10 60 98 114 32 47 62 10 60 104 114 32 47 62 10 60 98 114 32 47 62 10 116 104 105 115 32 100 97 115 104 32 119 105 108 108 32 114 101 110 100 101 114 32 97 115 32 97 32 39 104 114 39 32 116 97 103 46 60 98 114 32 47 62 10 60 98 114 32 47 62 10 45 60 98 114 32 47 62 10 60 98 114 32 47 62 10 116 104 105 115 32 104 121 112 104 101 110 32 115 104 111 117 108 100 32 110 111 116 60 98 114 32 47 62 10 226 148 128 32 110 111 114 32 115 104 111 117 108 100 32 116 104 105 115 32 108 105 110 101 32 115 116 97 114 116 105 110 103 32 119 105 116 104 32 111 110 101 60 98 114 32 47 62 10
	)) ;; }}}
)) ; }}}

(defun formatting-test-mdli () ; {{{
	(let ((entry  (format-body-html  
		(example-page-file
			:id 1610173599  :title "list"  :body
			'("* item 1" "* item 2" "" "this should render as a list")
			:bodyrules '("md-li")
		) nil))  ; :one-line t
	)
	
	#| reference:
# list 1.08<br />
<br />
<li>item 1</li>
<li>item 2</li>
<br />
this should render as a list<br />
	|#
	(string-equals-bytes  entry (vector ; {{{
		35 32 108 105 115 116 32 49 46 48 56 60 98 114 32 47 62 10 60 98 114 32 47 62 10 60 108 105 62 105 116 101 109 32 49 60 47 108 105 62 10 60 108 105 62 105 116 101 109 32 50 60 47 108 105 62 10 60 98 114 32 47 62 10 116 104 105 115 32 115 104 111 117 108 100 32 114 101 110 100 101 114 32 97 115 32 97 32 108 105 115 116 60 98 114 32 47 62 10
	)) ;; }}}
)) ; }}}

(defun formatting-test-mdlicheck () ; {{{
	(let (
		(entry  (format-body-html  
		(example-page-file
			:id 1610173599  :title "list"  :body
			'("* item 1" "* X item 2" "" "this should render as a list with a checkmark")
			:bodyrules '("md-li-x")
		) nil))  ; :one-line t
	)
	
	#| reference:
# list 1.08<br />
<br />
* item 1<br />
<li class=\"checked\"><b class=\"skeuo\">🗴</b> item 2</li>
<br />
this should render as a list with a checkmark<br />
	|#
	(string-equals-bytes  entry (vector ; {{{
		35 32 108 105 115 116 32 49 46 48 56 60 98 114 32 47 62 10 60 98 114 32 47 62 10 42 32 105 116 101 109 32 49 60 98 114 32 47 62 10 60 108 105 32 99 108 97 115 115 61 34 99 104 101 99 107 101 100 34 62 60 98 32 99 108 97 115 115 61 34 115 107 101 117 111 34 62 240 159 151 180 60 47 98 62 32 105 116 101 109 32 50 60 47 108 105 62 10 60 98 114 32 47 62 10 116 104 105 115 32 115 104 111 117 108 100 32 114 101 110 100 101 114 32 97 115 32 97 32 108 105 115 116 32 119 105 116 104 32 97 32 99 104 101 99 107 109 97 114 107 60 98 114 32 47 62 10
	)) ;; }}}
)) ; }}}

;;; }}}


;;; complex {{{

(defun formatting-test-mdpre-complex () ; test for actual bugged pre entries {{{
	;; there was a bug where 'rem' comment lines (;) were getting missed if 1 char long
	;; and another where pre tags weren't closed - still BUG , cause unknown
	(rules:find-meta-rules)
	(let ((entry "")  (entry-str ; entry-str {{{
"# A TABLE OF STUFF

```
ID           LABEL     INFO
             root      root node
1293030003             a page with zero title
1598380539   initial   initial commit
```

; =============================================
; many many many comments
; beebadobop
; ===========================================
;
; yup yup
; 
<= root
# md-pre"
	)) ; }}}
	
	(setq entry (format-body-html
		(parse-page-lines
			(uiop:split-string entry-str :separator (nn))
			:path (make-pathname :name "1293030003" :type "txt")
		)
	nil))
	
	#| reference:
<h1>A TABLE OF STUFF</h1>
<br />
<pre>ID           LABEL     INFO
             root      root node
1293030003             a page with zero title
1598380539   initial   initial commit
</pre>
	|#
	(string-equals-bytes  entry (vector ; {{{
		60 104 49 62 65 32 84 65 66 76 69 32 79 70 32 83 84 85 70 70 60 47 104 49 62 10 60 98 114 32 47 62 10 60 112 114 101 62 73 68 32 32 32 32 32 32 32 32 32 32 32 76 65 66 69 76 32 32 32 32 32 73 78 70 79 10 32 32 32 32 32 32 32 32 32 32 32 32 32 114 111 111 116 32 32 32 32 32 32 114 111 111 116 32 110 111 100 101 10 49 50 57 51 48 51 48 48 48 51 32 32 32 32 32 32 32 32 32 32 32 32 32 97 32 112 97 103 101 32 119 105 116 104 32 122 101 114 111 32 116 105 116 108 101 10 49 53 57 56 51 56 48 53 51 57 32 32 32 105 110 105 116 105 97 108 32 32 32 105 110 105 116 105 97 108 32 99 111 109 109 105 116 10 60 47 112 114 101 62 10
	)) ;; }}}
)) ; }}}

;;; }}}


(test formatting
	(is (formatting-test-h1) "rule h1")
	(is (formatting-test-h2) "rule md-h2")
	(is (formatting-test-uniquote) "rule uni-quote")
	(is (formatting-test-unihr) "rule uni-hr")
	(is (formatting-test-mdli) "rule md-li")
	(is (formatting-test-mdlicheck) "rule md-li-x")
	(is (formatting-test-mdpre) "rule md-pre")
	
	;;:depends-on (and 'rendering)
)
(test formatting-complex
	(is (formatting-test-mdpre-complex) "rule md-pre-complex")
	:depends-on (and 'formatting)
)


(defun run-tests ()
	(run! 'formatting-tests)
)
