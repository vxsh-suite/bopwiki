(defpackage :bopwiki-test-render-atom ; {{{
	(:use :common-lisp :bopwiki :fiveam :bop-render
		:bopwiki-test-helpers
		
		:bop-render-atom
		:bop-render-atom-author
		:bop-render-atom-page
	)
	(:local-nicknames
		(:table :bopwiki)  ; LATER: try with precise package
		(:authorr :bop-render-author)
	)
	(:export
		;; atom-number-bug atom-meta-test atom-entry-test
		#:run-tests
))
(in-package :bopwiki-test-render-atom)

(def-suite render-atom-tests)  ; fiveam
(in-suite render-atom-tests)
;;; }}}


(defun atom-number-bug () ; {{{
	(let (meta feed-created-str feed-created update-date updated-str)
	
	(setq
		feed-created (iso-to-epoch nil)
		feed-created-str
		(if (null feed-created)
			""  (epoch-to-iso feed-created :date-only t))
		
		update-date feed-created
		updated-str
		(if (null update-date)
			""  (epoch-to-iso update-date :date-only t))
	)
	
	(equal updated-str "")
)) ; }}}

(defun atom-meta-test () "test rendering feed metadata" ; {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	(let (meta (output "") (actor-file ; {{{
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>
<rdf:RDF
	xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"
	xmlns:b=\"http://distributary.network/owl-wing/2021-06/bop#\">

<b:cite rdf:ID=\"author_at_example.com-cite\">
	<b:created>2019-07-31T11:49:14Z</b:created>
	<b:username>author</b:username>
	<b:host>example.com</b:host>
	<b:title>Example Blog</b:title>
	<b:displayName>Author</b:displayName>
</b:cite>

</rdf:RDF>"
	)) ; }}}
	
	;; cache page body under "1_actor" {{{
	(table:put-page (bop-page-tablerow
		(table:put-page-body
			(example-page-file
				:id 1 :title "actor"
				:tags nil :bodyrules nil
				:path (make-pathname :name "1_actor" :type "xml"
					:directory '(:absolute "path" "to" "actor")
				)
				:body actor-file
			)))
	)
	;; }}}
	;; LATER: allow 'put-page'-ing a bop-page to do this
	
	(setq meta (atom-feed-meta :bop-version "20200222+3x4mpl3"))
	(setq output (atom-meta-template meta))
	
	#| reference:
<feed xmlns="http://www.w3.org/2005/Atom">
<id>tag:valenoern@distributary.network,2003-12-13:feed/atom</id>
<updated>2010-12-22T15:00:03.000Z</updated>
<title>bopwiki Atom feed — valenoern@distributary.network</title>
<author>
	<name>valenoern</name>
	<uri>http://distributary.network/bop/valenoern/</uri>
</author>
<link rel="self" href="http://distributary.network/bop/valenoern/feed" />
<generator uri="https://codeberg.org/Valenoern/bopwiki" version="20200222+3x4mpl3">bopwiki</generator>
	|#
	(string-equals-bytes  output  (vector ; {{{
		60 105 100 62 116 97 103 58 97 117 116 104 111 114 64 101 120 97 109 112 108 101 46 99 111 109 44 50 48 49 57 45 48 55 45 51 49 58 102 101 101 100 47 97 116 111 109 60 47 105 100 62 10 60 117 112 100 97 116 101 100 62 50 48 49 57 45 48 55 45 51 49 84 49 49 58 52 57 58 49 52 46 48 48 48 90 60 47 117 112 100 97 116 101 100 62 10 60 116 105 116 108 101 62 98 111 112 119 105 107 105 32 65 116 111 109 32 102 101 101 100 32 226 128 148 32 97 117 116 104 111 114 64 101 120 97 109 112 108 101 46 99 111 109 60 47 116 105 116 108 101 62 10 60 97 117 116 104 111 114 62 10 9 60 110 97 109 101 62 97 117 116 104 111 114 60
		47 110 97 109 101 62 10 9 60 117 114 105 62 104 116 116 112 58 47 47 101 120 97 109 112 108 101 46 99 111 109 47 98 111 112 47 97 117 116 104 111 114 47 60 47 117 114 105 62 10 60 47 97 117 116 104 111 114 62 10 60 108 105 110 107 32 114 101 108 61 34 115 101 108 102 34 32 104 114 101 102 61 34 104 116 116 112 58 47 47 101 120 97 109 112 108 101 46 99 111 109 47 98 111 112 47 97 117 116 104 111 114 47 97 116 111 109 46 120 109 108 34 32 47 62 10 60 103 101 110 101 114 97 116 111 114 32 117 114 105 61 34 104 116 116 112 115 58 47 47 99 111 100 101 98 101 114 103 46 111 114 103 47 86 97 108 101 110 111 101 114
		110 47 98 111 112 119 105 107 105 34 32 118 101 114 115 105 111 110 61 34 50 48 50 48 48 50 50 50 43 51 120 52 109 112 108 51 34 62 98 111 112 119 105 107 105 60 47 103 101 110 101 114 97 116 111 114 62
	)) ; }}}
)) ; }}}

(defun atom-entry-test () "test rendering a single atom feed entry" ; {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	(let (sample-page entry
		(meta  (make-feed-meta ; {{{
		:title "Test Feed"
		:authorname "Author Name"  :authorpath "http://example.com/bop/user/"
		:username "user"  :domain "example.com"
		:tag "tag:foo"  :link "feedpath"
		:updated 1598380539
		;; entire first part of tag uri before item name
		:namespace  (atom-tag-uri "user" "example.com" "2020-08-25" "")
		:pagesdir "http://example.com/bop/user/"
		)) ; }}}
	)
	(setq sample-page (read-example-page "1598380539_initial"))
	(setq entry (render-entry-atom "1598380539_initial"  :feed-meta meta :body sample-page))
	
	#| reference:
<entry>
	<id>tag:user@example.com,2020-08-25:1598380539</id>
	<title>initial 8.25</title>
	<link rel="alternate" type="text/html" href="http://example.com/bop/user/1598380539_initial.htm" />
	<published>2020-08-25T18:35:39.000Z</published>
	<updated>2020-08-25T18:35:39.000Z</updated>
	<summary type="xhtml"><div xmlns="http://www.w3.org/1999/xhtml"><h1>initial 8.25</h1>&#10;<br />&#10;commit d31532449696d1232ef1ccb7c0746eaa162a1521<br />&#10;Author: Takumi <takumi@sunrise-peak><br />&#10;Date:   Tue Aug 25 02:35:39 2020 -0800<br />&#10;<br />&#10;    initial commit; new.sh<br />&#10;</div></summary>
</entry>
	|#
	(string-equals-bytes entry  (vector ; {{{
		10 10 60 101 110 116 114 121 62 10 9 60 105 100 62 116 97 103 58 117 115 101 114 64 101 120 97 109 112 108 101 46 99 111 109 44 50 48 50 48 45 48 56 45 50 53 58 49 53 57 56 51 56 48 53 51 57 60 47 105 100 62 10 9 60 116 105 116 108 101 62 105 110 105 116 105 97 108 32 56 46 50 53 60 47 116 105 116 108 101 62 10 9 60 108 105 110 107 32 114 101 108 61 34 97 108 116 101 114 110 97 116 101 34 32 116 121 112 101 61 34 116 101 120 116 47 104 116 109 108 34 32 104 114 101 102 61 34 104 116 116 112 58 47 47 101 120 97 109 112 108 101 46 99 111 109 47 98 111 112 47 117 115 101 114 47 49 53 57 56 51 56 48 53 51 57 95 105 110 105 116 105 97 108 46 104 116 109 34 32 47 62 10 9 60 112 117 98 108
		105 115 104 101 100 62 50 48 50 48 45 48 56 45 50 53 84 49 56 58 51 53 58 51 57 46 48 48 48 90 60 47 112 117 98 108 105 115 104 101 100 62 10 9 60 117 112 100 97 116 101 100 62 50 48 50 48 45 48 56 45 50 53 84 49 56 58 51 53 58 51 57 46 48 48 48 90 60 47 117 112 100 97 116 101 100 62 10 9 60 115 117 109 109 97 114 121 32 116 121 112 101 61 34 120 104 116 109 108 34 62 60 100 105 118 32 120 109 108 110 115 61 34 104 116 116 112 58 47 47 119 119 119 46 119 51 46 111 114 103 47 49 57 57 57 47 120 104 116 109 108 34 62 60 104 49 62 105 110 105 116 105 97 108 32 56 46 50 53 60 47 104 49 62 38 35 49 48 59 60 98
		114 32 47 62 38 35 49 48 59 99 111 109 109 105 116 32 100 51 49 53 51 50 52 52 57 54 57 54 100 49 50 51 50 101 102 49 99 99 98 55 99 48 55 52 54 101 97 97 49 54 50 97 49 53 50 49 60 98 114 32 47 62 38 35 49 48 59 65 117 116 104 111 114 58 32 84 97 107 117 109 105 32 60 116 97 107 117 109 105 64 115 117 110 114 105 115 101 45 112 101 97 107 62 60 98 114 32 47 62 38 35 49 48 59 68 97 116 101 58 32 32 32 84 117 101 32 65 117 103 32 50 53 32 48 50 58 51 53 58 51 57 32 50 48 50 48 32 45 48 56 48 48 60 98 114 32 47 62 38 35 49 48 59 60 98 114 32 47 62 38 35 49 48 59 32 32 32 32 105 110 105 116 105 97 108 32 99 111 109 109 105 116 59 32 110 101 119 46 115 104 60 98 114 32 47 62 38 35 49 48 59 60 47 100 105 118 62 60 47 115 117 109 109 97 114 121 62 10 60 47 101 110 116 114 121 62
	)) ; }}}
)) ; }}}

;; BUG: something between owl-wings and atom-feed-meta causes a bug in automatic tests
;; #<TYPE-ERROR expected-type: NUMBER datum: NIL>


;;; run tests {{{
(test atomfeed
	(is (atom-number-bug) "Atom updated date bug")
	;;(is (atom-meta-test)  "Render atom feed metadata")  ; BUG
	(is (atom-entry-test) "Render atom entry")
	
	;;:depends-on (and 'rendering)
)

(defun run-tests ()
;; exists so I don't have to mess with fiveam symbols in main tests package
	(run! 'render-atom-tests)
)
;;; }}}
