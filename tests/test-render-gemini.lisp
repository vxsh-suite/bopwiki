(defpackage :bopwiki-test-render-gemini ; {{{
	(:use :common-lisp :bopwiki :fiveam :bop-render
		:bopwiki-test-helpers
		
		:bop-render-gemini-page
	)
	(:local-nicknames
		(:table :bopwiki)  ; LATER: try with precise package
	)
	(:export
		;; entry-without-meta entry-with-meta entry-raw-source
		#:run-tests
))
(in-package :bopwiki-test-render-gemini)

(def-suite render-gemini-tests)  ; fiveam
(in-suite render-gemini-tests)
;;; }}}


(defun entry-without-meta () ; with no meta lines {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	(let (full-body entry)
	
	(setq full-body (read-example-page "1598380539_initial"))
	(setq entry (render-entrypage-gemini 1598380539  :body full-body))
	
	#| reference:
commit d31532449696d1232ef1ccb7c0746eaa162a1521
Author: Takumi <takumi@sunrise-peak>
Date:   Tue Aug 25 02:35:39 2020 -0800

    initial commit; new.sh


```metadata
____
```

25/08/2020 10:35 - 1598380539


=> 1598380539_source.gmi  View entry source (not text-to-speech friendly)
	|#
	(string-equals-bytes  entry (vector ; {{{
		99 111 109 109 105 116 32 100 51 49 53 51 50 52 52 57 54 57 54 100 49 50 51 50 101 102 49 99 99 98 55 99 48 55 52 54 101 97 97 49 54 50 97 49 53 50 49 10 65 117 116 104 111 114 58 32 84 97 107 117 109 105 32 60 116 97 107 117 109 105 64 115 117 110 114 105 115 101 45 112 101 97 107 62 10 68 97 116 101 58 32 32 32 84 117 101 32 65 117 103 32 50 53 32 48 50 58 51 53 58 51 57 32 50 48 50 48 32 45 48 56 48 48 10 10 32 32 32 32 105 110 105 116 105 97 108 32 99 111 109 109 105 116 59 32 110 101 119 46 115 104 10 10 10 96 96 96 109 101 116 97 100 97 116 97 10 95 95 95 95 10 96 96 96 10 10 50 53 47 48 56 47 50 48
		50 48 32 49 48 58 51 53 32 45 32 49 53 57 56 51 56 48 53 51 57 10 10 10 61 62 32 49 53 57 56 51 56 48 53 51 57 95 115 111 117 114 99 101 46 103 109 105 32 32 86 105 101 119 32 101 110 116 114 121 32 115 111 117 114 99 101 32 40 110 111 116 32 116 101 120 116 45 116 111 45 115 112 101 101 99 104 32 102 114 105 101 110 100 108 121 41
	)) ;:dump t }}}
)) ; }}}

(defun entry-with-meta () ; with meta lines {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	(let (full-body entry)
	
	;; put in excerpting test entry
	(put-page (bop-page-tablerow (read-example-page "1620189405_excerpting")))
	
	(setq full-body (read-example-page "1633848808_metas"))
	(setq entry (render-entrypage-gemini 1633848808  :body full-body))
	
	#| reference:
meta line test


```metadata
____
```

=> 1293030003.gmi  [=>] 1293030003
=> 1620189405_excerpting.gmi  [=>] 1620189405
=> 1598380539_initial.gmi  [<=] 1598380539

09/10/2021 22:53 - 1633848808


=> 1633848808_source.gmi  View entry source (not text-to-speech friendly)
	|#
	(string-equals-bytes  entry (vector ; {{{
		109 101 116 97 32 108 105 110 101 32 116 101 115 116 10 10 10 96 96 96 109 101 116 97 100 97 116 97 10 95 95 95 95 10 96 96 96 10 10 61 62 32 49 50 57 51 48 51 48 48 48 51 46 103 109 105 32 32 91 61 62 93 32 49 50 57 51 48 51 48 48 48 51 10 61 62 32 49 54 50 48 49 56 57 52 48 53 95 101 120 99 101 114 112 116 105 110 103 46 103 109 105 32 32 91 61 62 93 32 49 54 50 48 49 56 57 52 48 53 10 61 62 32 49 53 57 56 51 56 48 53 51 57 95 105 110 105 116 105 97 108 46 103 109 105 32 32 91 60 61 93 32 49 53 57 56 51 56 48 53 51 57 10 10 48 57 47 49 48 47 50 48 50 49 32 50 50 58 53 51 32 45 32 49 54 51 51 56 52 56 56 48 56 10 10 10 61 62 32 49 54 51 51 56 52 56 56 48 56 95 115 111 117 114 99 101
		46 103 109 105 32 32 86 105 101 119 32 101 110 116 114 121 32 115 111 117 114 99 101 32 40 110 111 116 32 116 101 120 116 45 116 111 45 115 112 101 101 99 104 32 102 114 105 101 110 100 108 121 41
	)) ;:dump t }}}
)) ; }}}

(defun entry-raw-source () ; {{{
	(find-example-pages)  ; create page tables within bopwiki-core-table
	(let (full-body entry)
	
	(setq full-body (read-example-page "1598380539_initial"))
	(setq entry (raw-page-gemini full-body))
	
	#| reference:
# Entry source

```
commit d31532449696d1232ef1ccb7c0746eaa162a1521
Author: Takumi <takumi@sunrise-peak>
Date:   Tue Aug 25 02:35:39 2020 -0800

    initial commit; new.sh

```

=> 1598380539_initial.gmi  Back to page
	|#
	(string-equals-bytes  entry (vector ; {{{
		35 32 69 110 116 114 121 32 115 111 117 114 99 101 10 10 96 96 96 10 99 111 109 109 105 116 32 100 51 49 53 51 50 52 52 57 54 57 54 100 49 50 51 50 101 102 49 99 99 98 55 99 48 55 52 54 101 97 97 49 54 50 97 49 53 50 49 10 65 117 116 104 111 114 58 32 84 97 107 117 109 105 32 60 116 97 107 117 109 105 64 115 117 110 114 105 115 101 45 112 101 97 107 62 10 68 97 116 101 58 32 32 32 84 117 101 32 65 117 103 32 50 53 32 48 50 58 51 53 58 51 57 32 50 48 50 48 32 45 48 56 48 48 10 10 32 32 32 32 105 110 105 116 105 97 108 32 99 111 109 109 105 116 59 32 110 101 119 46 115 104 10 96 96 96 10 10 61 62 32 49 53 57
		56 51 56 48 53 51 57 95 105 110 105 116 105 97 108 46 103 109 105 32 32 66 97 99 107 32 116 111 32 112 97 103 101 10
	;; ) :dump t)
	)) ;}}}
)) ; }}}

;; LATER: tests for root entry with and without date


(test gemini ; {{{
	(is (entry-without-meta) "render simple entry")
	(is (entry-with-meta) "render entry with meta lines")
	(is (entry-raw-source) "render 'entry source' page")
	
	;;:depends-on (and 'rendering)
) ; }}}

(defun run-tests ()
;; exists so I don't have to mess with fiveam symbols in main tests package
	(run! 'render-gemini-tests)
)
