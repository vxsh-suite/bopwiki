;;; tests in "rendering" suite {{{
(defpackage :bopwiki-test-render
	(:use :common-lisp :bopwiki :bop-render :fiveam :bopwiki-test-helpers
	
		;; LATER: fix re-exporting
		:bop-render-html-2
		:bop-render-html-copy
		:bop-render-html-page
	)
	(:local-nicknames
		(:table :bopwiki)
		(:html :bop-render-html-2)
	)
	(:export
		#:example-directories
		
		#:url-test
		#:group-directory-test
		#:publish-target-query-test #:publish-target-test
		
		#:run-tests
))
(in-package :bopwiki-test-render)

(def-suite render-core-tests)  ; fiveam
(in-suite render-core-tests)
;;; }}}


;;; render core {{{

(defun url-test () ; webpage urls using example-pagetable {{{
    ;; make sure webpage urls in html/gmi export are constructed correctly
    
    (find-example-pages)  ; create page tables within bopwiki-core-table
    
	(let (
		(zero (exported-page-url "1293030003" "htm"))
		(initial (exported-page-url "1598380539" "htm"))
		;; id numbers are integers inside table rows,
		;; so we should test passing integers also
		(zero-two (exported-page-url 1293030003 "htm"))
		(initial-two (exported-page-url 1598380539 "htm"))
	)
	
	(and
		(equal zero (name-page-str "1293030003" nil "htm"))
		(equal initial (name-page-str "1598380539" "initial" "htm"))
		(equal zero zero-two)
		(equal initial initial-two)
	)
)) ; }}}

(defun group-directory-test () ; {{{
	(example-directories)  ; create example directories containing 5 pages
	(let ((result ""))
	
	(let (flattened
		(dirs (list "PATHS:" "/dir1/" "/dir2/" "/"))
		(dir1 (table:pages-from-directory (parse-namestring "/example/test.bop/dir1/")))
		(dir2 (table:pages-from-directory (parse-namestring "/example/test.bop/dir2/")))
		(top (table:pages-from-directory (parse-namestring "/example/test.bop/")))
		)
		
		;; flatten directories into one string
		(dolist (names-list (list dirs dir1 dir2 top))
			(setq flattened (cons "  LIST:" flattened))
			(dolist (page names-list)
				(setq flattened (cons page flattened))
		))
		(setq flattened (reverse flattened))
		
		;; flatten 
		(setq result (join-str-with flattened " "))
	)
	
	#| reference:
  LIST: PATHS: /dir1/ /dir2/ /   LIST: 1620977512_page3   LIST: 1620977694_page5 1620977583_page4   LIST: 1620977694_page5 1620977583_page4 1620977512_page3 1620977318_page2 1620977002_page1
	|#
	(string-equals-bytes  result  (vector ; {{{
		32 32 76 73 83 84 58 32 80 65 84 72 83 58 32 47 100 105 114 49 47 32 47 100 105 114 50 47 32 47 32 32 32 76 73 83 84 58 32 49 54 50 48 57 55 55 53 49 50 95 112 97 103 101 51 32 32 32 76 73 83 84 58 32 49 54 50 48 57 55 55 54 57 52 95 112 97 103 101 53 32 49 54 50 48 57 55 55 53 56 51 95 112 97 103 101 52 32 32 32 76 73 83 84 58 32 49 54 50 48 57 55 55 54 57 52 95 112 97 103 101 53 32 49 54 50 48 57 55 55 53 56 51 95 112 97 103 101 52 32 49 54 50 48 57 55 55 53 49 50 95 112 97 103 101 51 32 49 54 50 48 57 55 55 51 49 56 95 112 97 103 101 50 32 49 54 50 48 57 55 55 48 48 50 95 112 97 103 101 49
	)) ;; }}}
)) ; }}}

;; LATER: test that tag-pages returns correctly in testing environment
(defun publish-target-query-test () "test getting dir-pages for publish target" ; {{{
	(example-directories)  ; create example directories containing 5 pages
	(let ((dir-1 (dir-pages "dir1")) (dir-2 (dir-pages "dir2")) result-string)
	
	(setq result-string (format nil "~a~%~a" dir-1 dir-2))
	
	(string-equals-bytes  result-string  (vector ; {{{
		40 49 54 50 48 57 55 55 53 49 50 95 112 97 103 101 51 41 10 40 49 54 50 48 57 55 55 54 57 52 95 112 97 103 101 53 32 49 54 50 48 57 55 55 53 56 51 95 112 97 103 101 52 41
	)) ; }}}
)) ; }}}

(defun publish-target-test ()  ; test publish groups {{{
	(example-directories)  ; create example directories containing 5 pages
	(let ((res-list (list nil nil nil)))
	;(pwd (uiop:ensure-pathname "/example/test.bop/"))
	
	(register-publish-target :name "one"  :func 'html:publish-bop-html
		:data (list (dir-pages "dir1")))
	(register-publish-target :name "two"  :func 'html:publish-bop-html
		:data (list (dir-pages "dir2")))
	(register-publish-target :name "both"  :func 'html:publish-bop-html
		:data (list (dir-pages "dir1") (dir-pages "dir2")))
	
	;; for each publish target, get pages
	(let ((dirs (list "one" "two" "both")) (res-number 0))
	(dolist (dir dirs)
		(let ((target (parse-wants dir)))
		
		(setf (nth res-number res-list) (format nil "PUBLISH-TARGET - PAGES ~a"  (publish-target-pages target)))
		
		(setq res-number (+ res-number 1))
	)))
	
	#|
(PUBLISH-TARGET - DIRS NIL - PAGES (1620977512_page3)
 PUBLISH-TARGET - DIRS NIL - PAGES (1620977694_page5 1620977583_page4)
 PUBLISH-TARGET - DIRS (dir1 dir2) - PAGES (1620977512_page3 1620977694_page5 1620977583_page4))
	|#
	(string-equals-bytes  (format nil "~a" res-list)  (vector ; {{{
		40 80 85 66 76 73 83 72 45 84 65 82 71 69 84 32 45 32 80 65 71 69 83 32 40 49 54 50 48 57 55 55 53 49 50 95 112 97 103 101 51 41 10 32 80 85 66 76 73 83 72 45 84 65 82 71 69 84 32 45 32 80 65 71 69 83 32 40 49 54 50 48 57 55 55 54 57 52 95 112 97 103 101 53 32 49 54 50 48 57 55 55 53 56 51 95 112 97 103 101 52
		41 10 32 80 85 66 76 73 83 72 45 84 65 82 71 69 84 32 45 32 80 65 71 69 83 32 40 49 54 50 48 57 55 55 54 57 52 95 112 97 103 101 53 32 49 54 50 48 57 55 55 53 56 51 95 112 97 103 101 52 32 49 54 50 48 57 55 55 53 49 50 95 112 97 103 101 51 41 41
	)) ;; }}}
)) ; }}}

;;; }}}


(test rendering
	(is (url-test) "Create webpage urls correctly")
	(is (group-directory-test) "Group pages into directories")
	
	(is (publish-target-query-test) "Publish target data test")
	(is (publish-target-test) "Publish group test")
	
	;;:depends-on (and 'basic)
)


(defun run-tests ()
;; exists so I don't have to mess with fiveam symbols in main tests package
	(run! 'render-core-tests)
)
;; BUG: sbcl confuses this with the atom feed one for unknown reasons? i don't see any typos
