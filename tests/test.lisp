;;; test.lisp {{{
;; These are for making sure the program is working & documented. {{{
;; they should check against what the program is known to output even if it's 'wrong',
;; to make it easy to check for unexpected changes in how the program works.
;; 
;; Any explanation of /why/ the output is wrong should be documented in the source file that code is actually in (usually as LATER or BUG), but may be marked here as 'wrong' and/or what file / issues.bop entry to reference.
;; }}}

(defpackage :bopwiki-test
	(:use :common-lisp :bopwiki :bop-render
		;; LATER: see .asd file for which files are contained in which packages
		:bopwiki-test-helpers
	)
	(:local-nicknames
		(:bop :bopwiki)
		
		(:core :bopwiki-test-core)
		(:fmt :bopwiki-test-formatting)
		(:extns :bopwiki-test-extensions)
		
		(:render :bopwiki-test-render)
		(:html :bopwiki-test-render-html)
		(:atom :bopwiki-test-render-atom)
		(:gmi :bopwiki-test-render-gemini)
	)
	(:export
		#:test-bop
))
(in-package :bopwiki-test)
;;; }}}


(defun test-bop (&key pwd wants action) ; {{{
	pwd wants action  ; unused required arguments
	
	;; LATER: make suite dependencies work again
	
	(core:run-tests)  ; OK 22-1-05
	(fmt:run-tests)  ; OK 22-1-05
	(extns:run-tests)  ; OK 22-1-05
	(render:run-tests)  ; OK 22-1-05
	(html:run-tests)  ; OK 22-1-05
	(gmi:run-tests)  ; OK 22-1-05
	(atom:run-tests)  ; OK 22-1-05
	
	t
) ; }}}

(bop:register-command "test" 'test-bop)
