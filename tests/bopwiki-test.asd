(defsystem "bopwiki-test"
;	:version "0.0.1"
	:description "bop microwiki - test suites"
	:author "Valenoern"
	:licence "GPL"
	
	:depends-on ("uiop" "bopwiki" "bop-render" "fiveam"
		"bop-render-publishgroup"
		"bop-render-html" "bop-render-atom" "bop-render-gemini" ;"bop-render-activitypub"
	)
	:components (
		;; helper functions
		(:file "test-helpers")
		
		;; actual tests
		(:file "test-core" :depends-on ("test-helpers"))
		(:file "test-extensions" :depends-on ("test-render"))
		(:file "test-formatting" :depends-on ("test-helpers"))
		
		
		;; render tests
		(:file "test-render"
			:depends-on ("test-helpers"))
		
		(:file "test-render-html"
			:depends-on ("test-helpers"))
		
		(:file "test-render-atom"
			:depends-on ("test-helpers"))
		
		(:file "test-render-gemini"  ; :bopwiki-test-render-gemini
			:depends-on ("test-helpers"))
		
		
		;; "bop test" command
		(:file "test" :depends-on ("test-helpers" "test-core" "test-render" "test-formatting"
			 "test-render-html"  "test-render-atom"))
))

;; LATER: package fiveam for manjaro
;; dependencies - :alexandria :net.didierverna.asdf-flv :trivial-backtrace
