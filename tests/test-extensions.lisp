(defpackage :bopwiki-test-extensions ; {{{
	(:use :common-lisp :bopwiki :fiveam
		:bop-render :bop-render-publishgroup
		:bopwiki-test-helpers
	)
	(:local-nicknames
		(:html :bop-render-html-2)
	)
	(:export
		;; #:pubgroup-test
		#:run-tests
))
(in-package :bopwiki-test-extensions)

(def-suite extension-tests)
(in-suite extension-tests)
;;; }}}


(defun pubgroup-test () ; {{{
	(example-directories)  ; create example directories containing 5 pages
	(let (target-res group-res)
	group-res ; leave dummy alone
	;(pwd (uiop:ensure-pathname "/example/test.bop/"))
	
	;; will not be tested
	(register-publish-target :name "one" :func 'html:publish-bop-html
		:data (list (dir-pages "dir1")))
	(register-publish-target :name "two" :func 'html:publish-bop-html
		:data (list (dir-pages "dir2")))
	
	;; will compare a regular combined publish target with a publish group
	(register-publish-target :name "both" :func 'html:publish-bop-html
		:data (list (dir-pages "dir1") (dir-pages "dir2")))
	(register-publish-group :name "both2"  :targets (list "one" "two"))
	
	;; render list of pages from publish target
	(let (both)
		(setq both (parse-wants "both"))
		(setq target-res (format nil "PUBLISH-TARGET - PAGES ~a" (publish-target-pages both)))
	)
	(let (group target-list res (res-list nil))
		(setq group (parse-wants "both2"))
		(setq target-list (publish-group-data group))  ; process each target
		
		(dolist (target target-list)
			(setq res (publish-target-pages target))
			(setq res-list (append res res-list))
		)
		(setq group-res (format nil "PUBLISH-TARGET - PAGES ~a" res-list))
	)
	
	(equal target-res group-res)
)) ; }}}


(test pubgroups
	(is (pubgroup-test) "Publish groups")
)

(defun run-tests ()
	(run! 'extension-tests))
