;;; stylesheet.lisp - generate default stylesheet {{{
(defpackage :bop-render-html-style
	(:use :common-lisp)
	(:local-nicknames
		(:table :bopwiki-core-table)
	)
	(:export
		#:*html-body-colour* #:*html-entry-colour*
		#:*html-border-colour* #:*html-nesting-colour*
		
		#:html-stylesheet-colours
		;; html-stylesheet-breakpoints
		#:html-stylesheet-extra
		#:html-stylesheet
		
		#:colour-scheme-default
		#:colour-scheme-distributary #:colour-scheme-rose #:colour-scheme-lavender
))
(in-package :bop-render-html-style)

(defparameter *html-body-colour*    nil)
(defparameter *html-entry-colour*   nil)
(defparameter *html-border-colour*  nil)
(defparameter *html-nesting-colour* nil)

;;; }}}


;;; templates {{{

(defun html-stylesheet-colours (&key body entry border nesting) ; {{{
;; to change the colours you have three options:
;; 1. set the global variables  2. swap 'html-stylesheet-colours with a lambda calling it with new colours  3. swap 'html-stylesheet-colours with a totally new function

	(let ()
		(format nil
"/* colour */

body, article { background: ~a; }

article { background: ~a; }

article, .attachments figure { border-color: ~a; }

.nesting { color: ~a; }
"
;; "body, article" note:
;; Firefox's inspector didn't make it easy to edit the styles on both body and article at the same time, so this is a simple hack to allow you to click "article" and tweak both of them.

		;; body
		(or body *html-body-colour* "#cdd2cb")
		;; article
		(or entry *html-entry-colour* "rgba(255, 255, 255, 0.73)")
		;; border
		(or border *html-border-colour* "#686868")
		;; nesting
		(or nesting *html-nesting-colour* *html-border-colour* "#686868")
		
))) ; }}}

(defun html-stylesheet-breakpoints () ; {{{

"/* adjustments for small screens - mostly to condense excess space */

/* on big screens, hide nesting glyph */
@media only screen and (min-width: 651px) {
  .nesting { display: none; }
}

/* on medium-size screens, shorten vertical spaces */
@media only screen and ((max-height: 600px) or (max-width: 850px)) {
  .replies { margin: 1.0em 0.8em 1.2em 1em; }
  .replies article { margin: 0.9em 0; }
}

/* on thin screens, shorten horizontal margins */
@media only screen and (max-width: 650px) {
  body { padding: 7px; }
  .replies { margin: 0 7px 1.4em 9px; }
  
  .nesting { font-size: 85%; margin: 0.6em 0 1.2em 2px; }
}
@media only screen and (max-width: 430px) {
  body { padding: 2px; }
  article { padding: 9px; }
  
  /* make horizontal margins fixed, keep vertical ones proportional to text */
  .replies { margin: 0 0 1.4em 0; }
  .replies article { margin: 0.8em 0; }
}
"

) ; }}}

;; a format or your publish target etc may add extra css by registering an alternate for this function
(defun html-stylesheet-extra () "")

(defun html-stylesheet () ; default stylesheet {{{
	(let (
		(stylesheet-extras (table:get-alternate-function 'html-stylesheet-extra))
	)
	
	(format nil
"body { padding: 15px; }

article { padding: 1em; border: 1px solid; }

.replies { margin: 1em 1em; margin-right: 0.8em; }
.replies article { margin: 1.5em 0; }

main > hr, .replies > hr { display: none; } /* entry separators */

.skeuo,  .copy li:before, .copy hr:after
{ font-weight: bold; font-size: 1.1em; }


/* body ('copy') */

.copy { line-height: 1.46; }

.copy h1, .copy h2, .copy h3 { margin: 0; }
.copy h1 { font-size: 1.5em; }
.copy h2 { font-size: 1.2em; }

.copy .cut { font-size: 130%; font-style: italic; }

.copy blockquote { margin: 0.3em 0 0.3em 1.2em; }
.copy pre { line-height: 1; }

.copy hr { border: 0; margin: 0; font-weight: bold; }
.copy hr:after { content: \"─\"; }

.copy li { list-style-type: none; }
.copy li:before {
content: \"*\"; margin-right: 0.5em; vertical-align: middle;
font-family: sans-serif;
}
.copy li.checked:before { margin-right: 0; }
.copy li.checked > .skeuo { margin: 0 0.1em 0 0.3em; font-size: 1.05em; }


/* footer */

article footer {
margin-top: 2.5em;
font-size: 98%;
}

article .meta { margin-top: 1em; }

footer > p { margin: 0; }
footer time a, a.cut { text-decoration: none; }

article time b {
font-size: 130%;
vertical-align: middle; margin: 0;
}

article .meta b {
font-size: 120%; font-weight: normal; font-family: sans-serif;
}

article footer .entry-info-summarised {
font-size: 92%; margin-left: 0.7em;
}


/* media attachments */

footer .attachments {
display: flex; flex-wrap: wrap;
margin-top: 1em;
}

.attachments figure {
width: 16em;
border: 1px solid; padding: 7px; margin: 0;
display: inline-block;
overflow: hidden;
}
.attachments img {
height: 11em; max-width: 16em;
margin-top: 4px;
display: block;
}


~a

~a~A"
	(html-stylesheet-colours)  ; LATER: allow alternate and check it works
	(html-stylesheet-breakpoints)
	(funcall stylesheet-extras)
	)
)) ; }}}

;;; }}}


(defun colour-scheme-default () ; {{{
	;; simple white / grey colour scheme
	(setf
		*html-body-colour* "#cdd2cb"
		*html-entry-colour* "rgba(255, 255, 255, 0.73)"
		*html-border-colour* "#686868"
		)
) ; }}}

(defun colour-scheme-distributary () ; {{{
	;; colour scheme designed for distributary.network
	(setf
		*html-body-colour* "#d6fffe"
		*html-entry-colour* "rgba(33, 34, 141, .10)"
		*html-border-colour* "darkcyan"
		)
) ; }}}

(defun colour-scheme-rose () ; {{{
	;; light red sunset-looking colour scheme
	(setf
		*html-body-colour* "#fde9e9"
		*html-entry-colour* "rgba(182, 195, 200, 0.51)"
		*html-border-colour* "#01317d"
		)
) ; }}}

(defun colour-scheme-lavender () ; {{{
	;; lavender variation of 'colour-scheme-default
	(colour-scheme-default)
	(setf
		*html-body-colour* "#a69ba5")
) ; }}}
