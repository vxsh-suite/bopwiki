;;; entry-page.lisp - output full entry and/or thread {{{
(defpackage :bop-render-html-page
	(:use :common-lisp
		:bopwiki
			;; join-lines
		:bop-render-core
		:bop-render-html-copy
	)
	(:local-nicknames
		(:table :bopwiki) (:linerules :bopwiki)
		(:rrender :bopwiki-linerules-render)
		(:template :bop-render-html-template)
		(:render :bop-render-core)
		(:rrel :bop-render-rules)
		(:footer :bop-render-html-footer)
		;; uiop
	)
	(:export
		#:preview-table-key  ;*preview-table*
		
		#:render-entry-html #:render-replies-html
))
(in-package :bop-render-html-page)

;; LATER: start saving which entry previews are on page
(defparameter *preview-table* nil)

(defun preview-table-key (entry) ; {{{
	(format nil "~a" entry)
) ; }}}

;;; }}}


(defun render-entry-html (pagename &key replyp parent body nesting raw) ; render entry box {{{
"Render single entry within an html webpage of an entry or thread"

	(let ((id (table:page-id pagename)) meta-block page-date entry-header
		full-body body-str raw-source (summarised nil) intermediate
		(template-func (table:get-alternate-function 'template:html-entry-template))
		(meta-func (table:get-alternate-function 'footer:html-meta-block))
	)
	
	;; mainly for automated testing, accept full-body from args
	(setq full-body
		(if (null body)  ; if body was not passed in
			(page-body pagename :with-raw t)  ; load from file or cache
			body))
	
	;; entry excerpt, :summarised, :body
	;; BUG: excerpt-text is run twice between render body and here. fix that later
	(let (excerpt) ; {{{
		(setq excerpt (bop-page-excerpt full-body))
		
		;; add excerpt if not already present, as it might be during tests
		(when (null excerpt)
			(setf
				excerpt (render:excerpt-text full-body)
				(bop-page-excerpt full-body) excerpt))
		
		;; if excerpt already existed OR was added, mark entry summarised
		(setq summarised
			(and
				(will-excerpt-p full-body  :is-reply replyp)
				(not-null excerpt))
		)
	) ; }}}
	
	(setq  ; raw page-date meta-block body-str {{{
		;; reconstruct original source file
		raw-source (template:raw-page-html full-body replyp)
	
		;; show date of entry if date exists
		page-date (footer:html-page-date id)
		;; render meta block
		meta-block (funcall meta-func full-body replyp parent)
		
		;; evaluate and/or format entry
		body-str (render:process-entry-body 'format-body-html full-body replyp)
	) ; }}}
	
	(setq intermediate ; {{{
		(make-rendered-entry
			:id (format nil "~a" id)
			:header entry-header
			:body body-str  :raw raw-source
			:date page-date  :metablock meta-block
			:isreply replyp  :summarised summarised
			:nesting nesting
	)) ; }}}
	
	;; return raw 'rendered-entry if requested, otherwise output html string
	(if (null raw)
		(funcall template-func intermediate)  intermediate)
)) ; }}}

(defun render-replies-html (full-body  &key nesting) ; {{{
	(let (preview-links results ; row idno nesting-level template-func {{{
		(row (bop-page-tablerow full-body))
		(idno (bop-page-id full-body))
		(nesting-level  (if (null nesting) 0 nesting))
		(template-func (table:get-alternate-function 'template:html-entry-template))
	) ; }}}
	
	;; organise entries which will be displayed as replies
	(let ((outlinks (bop-tablerow-outlinks row)) threadlinks discard-replies) ; {{{
		(setq discard-replies (rrel:meta-previewless-p outlinks))
		
		(when (null discard-replies)
			(setq
				;; use outgoing links in the order they appear
				outlinks
					(linerules:meta-lines-with (bop-tablerow-outlinks row) 'line-rule-displayp)
			
				threadlinks  ; sort replies ascending
					(stable-sort
						(linerules:non-revision-metas (bop-tablerow-threadlinks row))
						#'string-lessp
						:key  #'(lambda (x) (format nil "~a" (meta-line-value x)))
						)
			)
		
			;; assume that you want to preview outlinks and then replies
			;; LATER: I think I forgot to reverse threadlinks while collecting them; correct in filters
			(setq preview-links
				(rrender:remove-duplicate-replies
					(reverse (append (reverse threadlinks) outlinks))
					))
			;; LATER: see if this innermost function should be swappable for a custom one after beta testing. previously bop displayed outlinks and replies mixed together and sorted ascending
	)) ; }}}
	
	;; render reply entries into 'results list
	(dolist (link preview-links) ; {{{
		(let ((link-idno (meta-line-value link)) link-row rule full-body intermediate
			further-replies excerpt-meta excerpt-row)
		(setq link-row (table:page-row link-idno))
		
		;; provided page exists...
		(when (page-exists-p nil :row link-row)
			(setq
				rule  (funcall (meta-line-kindsym link))  ; get meta line's rule
				
				full-body  ; get full entry now if it will be excerpted
					(if (line-rule-threadp rule)  (page-body link-idno)  nil)
				
				intermediate  ; render entry 'OP'
				(render-entry-html link-idno
					:replyp t :raw t :parent idno :nesting nesting-level :body full-body)
				
				;; add entry 'OP' to replies
				results  (cons (funcall template-func intermediate) results)
			)
			
			;; if this meta line is for threading - as with cxt/"<=" - recurse into further replies
			(when (line-rule-threadp rule)
				(unless  ; if the entry just rendered was manually summarised... {{{
					(or
						(null (rendered-entry-summarised intermediate))  (null full-body)
						(null
							(render:excerpt-visible-outlinks
								(setq excerpt (bop-page-excerpt full-body))
						))
					)
					(setf  ; ...replace its outlinks with its excerpt's outlinks
						excerpt-row (bop-page-tablerow excerpt)
						link-row
							(table:duplicate-bop-tablerow link-row
								:outlinks (bop-tablerow-outlinks excerpt-row))
						(bop-page-tablerow full-body) link-row
					)
				) ; }}}
				
				;; render threadlink's replies
				(setq further-replies
					(template:html-replies-section
						(render-replies-html full-body :nesting (+ nesting-level 1))
						))
				;; don't add empty strings to list (although it wouldn't matter hugely)
				(unless (null further-replies)
					(setq results (cons further-replies results)))
		))
		;; LATER: checking whether page was previewed in other levels has been removed for now
		;; I'm thinking the best way to do deduplication might actually involve walking the entire tree of which entries will be rendered (maybe in a tablepost-filter style thing?) before rendering anything
	)) ;; }}}
	
	(reverse results)  ; return list of results in forward order
)) ; }}}
