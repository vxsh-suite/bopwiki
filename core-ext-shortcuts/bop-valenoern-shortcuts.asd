(defsystem "bop-valenoern-shortcuts"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "shortcuts for init.lisp"
	
	:depends-on ("uiop" "bopwiki" "bop-render" "bop-render-html")
	:components (
		(:file "vs-render")  ; publish targets
		(:file "vs-query")  ; init.lisp query functions
))

;; experimental system to collect shortcut functions I keep copying across init.lisp files.
;; will not necessarily be stable across time; should be regarded more like a demo.
;; good ones that stay stable long enough may eventually be moved into core packages.
