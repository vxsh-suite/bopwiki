;; shortcuts mainly for bop-render
(defpackage :bop-valenoern-shortcuts-render
	(:use :common-lisp :bopwiki)
	(:local-nicknames
		(:render :bop-render)
		(:html :bop-render-html-2)
	)
	(:export
		;; publish targets
		#:publish-target #:publish-html
	)
)
(in-package :bop-valenoern-shortcuts-render)


(defun publish-target (name func data)
	(render:register-publish-target :name name  :data data  :func func)
)

(defun publish-html (name data)
	(render:register-publish-target :name name  :data data  :func 'html:publish-bop-html)
)
