;;; page-uris.lisp
(defpackage :bopwiki-uri
	(:use :common-lisp :bopwiki-util)
	(:export
		;; numeric IDs
		#:id-digit-p #:is-epoch #:is-iso-date #:is-valid-date
		#:iso-to-epoch #:epoch-to-iso #:date-to-epoch
		#:has-protocol
		
		;; basenames
		#:name-page-str #:name-page-lisp #:name-page
		#:page-file-p
		#:split-pagename
))
(in-package :bopwiki-uri)


;;; numeric page IDs {{{

;; check dates - id-digit-p, is-epoch, is-iso-date, is-valid-date {{{

(defun id-digit-p (chr) (and (char<= chr #\9) (char>= chr #\0)))

(defun is-epoch (str) ; {{{
"return whether STR is a unix epoch number, i.e. a number of seconds since 1970.
integer values are assumed to be valid dates; otherwise the string is tested for digits"
	;; LATER: explain
	(or (numberp str) (and
	(stringp str)
		(let ((not-digit t))
		(loop for chr across str do
			(unless (id-digit-p chr)
			(setq not-digit nil)
		))
		not-digit)
	)
)) ; }}}

(defun is-iso-date (str) ; disqualify invalid iso dates {{{
"return whether STR is 'probably' an ISO date.
this is mostly meant to disqualify obviously invalid strings versus test very closely"
	(let ((len (length str)))
	;; a string is 'probably' an ISO date if
	;; it is at least 8 characters long, AND
	(and  (>= len 8)  (or
		(and
			;; ends with (milli)seconds and a GMT time zone 00Z
			(equal (elt str (- len 1)) #\Z)
			(id-digit-p (elt str (- len 2)))
			(id-digit-p (elt str (- len 3)))
		)
		(and
			;; OR starts with a year such as 2020-
			(id-digit-p (elt str 0)) (id-digit-p (elt str 1))
			(id-digit-p (elt str 2)) (id-digit-p (elt str 3))
			(equal (elt str 4) #\-)
		)
	))
)) ; }}}

(defun is-valid-date (str) ; check if string is valid epoch or ISO {{{
	(or (is-epoch str) (is-iso-date str))
) ; }}}

;; }}}

;; convert date formats - iso-to-epoch, epoch-to-iso, date-to-epoch {{{

(defun iso-to-epoch (iso-time) ; convert iso to epoch integer {{{
;; doc {{{
"Attempt to parse ISO 8601 date as used in html time tag.
ISO-TIME must be a full date string in GMT, e.g. 2020-11-12T01:37:12Z or 2020-11-12T01:37:12.000Z (milliseconds are discarded).
This should usually return the same result as javascript's Date.getTime()/1000 ."
;; https://developer.mozilla.org/en-US/docs/Web/HTML/Date_and_time_formats
;; }}}
	(let ((len (length iso-time)) err year month day hour minute sec  prev-chrs epoch)
	
	;; first do a sanity check date is in correct format {{{
	;; a really sophisticated function could handle just a date or time
	;; but requiring both is simpler, & more useful as a unique id number
	(when (and
		(or (equal len 20) (equal len 24))  (equal (elt iso-time (- len 1)) #\Z)
	)
	;; }}}
	
	(loop for chr across iso-time  while (null err) do ; {{{
		(let (strng num (at-boundary
			;; assume the time should be divided if at one of these chars
			(or (equal chr #\-) (equal chr #\T) (equal chr #\:)
			(equal chr #\.) (equal chr #\Z))
		))
		;; if at boundary, try to convert previous segment to number
		(when (equal at-boundary t) ; {{{
			(setq strng (list-to-string (reverse prev-chrs)))
			(setq num (parse-integer strng :junk-allowed t))
			;; when using junk-allowed, invalid numbers will become nil
			;; this should not happen, so stop the function and return nil
			(when (null num) (setq err t))
		) ; }}}
		
		(cond
			((and (null year) (equal chr #\-))  (setq year num))
			((and (null month) (equal chr #\-))  (setq month num))
			((and (null day) (equal chr #\T))  (setq day num))
			((and (null hour) (equal chr #\:))  (setq hour num))
			((and (null minute) (equal chr #\:))  (setq minute num))
			((and (null sec)
				(or (equal chr #\.) (equal chr #\Z)))  (setq sec num))
			(t (setq prev-chrs (cons chr prev-chrs))
		))
		
		;; LATER: move to above WHEN block?
		(when (equal at-boundary t)  (setq prev-chrs nil))
	)) ; }}}
	
	;; if there are no problems with result, format it as epoch {{{
	(when (null err)
	;; Common Lisp has a native way to calculate with individual calendar numbers
	;; last arg is the time zone offset - GMT+0, the same as Z
	(let ((universal (encode-universal-time sec minute hour day month year 0)))
		;; Common Lisp universal time starts at 1900 instead of 1970
		;; but there's already a way to convert this to epoch time in util.lisp
		(setq epoch (universal-to-unix-time universal))
	))
	;; }}}
	
	epoch
))) ; }}}

(defun epoch-to-iso (epoch-time &key date-only) ; convert epoch integer to iso string {{{
	(multiple-value-bind
		(sec minute hour day month year) ; weekday dst-p tz
		(decode-universal-time (unix-to-universal-time epoch-time) 0)
		
		(cond
		;; if passed :date-only t , give YYYY-MM-DD , for use in tag: uris etc
		((equal date-only t)
			(format nil  "~a-~2,'0d-~2,'0d"  year month day))  ; hour minute sec
		;; else print iso date, ex. 2020-12-15T05:09:35.000Z
		(t  (format nil  "~a-~2,'0d-~2,'0dT~2,'0d:~2,'0d:~2,'0d.000Z"  year month day  hour minute sec))
))) ; }}}

(defun date-to-epoch (str) ; convert epoch or iso to iso {{{
"Convert STR to a unix epoch number if it is a valid iso or epoch date."
	(cond
		((is-epoch str) (parse-integer str :junk-allowed t))
		((is-iso-date str) (iso-to-epoch str))
		(t nil)
)) ; }}}

;; }}}

;; other URIs - has-protocol {{{

(defun has-protocol (raw-uri) "return whether link has protocol" ; {{{
	(let (len has-prt
		(uri (format nil "~a" raw-uri))  ; in case uri is integer, convert to str
	)
	(setq len (- (length uri) 1))
	
	(loop for i from 0 to len  while (null has-prt)  do
		;(let ((chr (elt uri i)))
		
		(when (and
			(equal (elt uri (+ i 0)) #\:)
			(equal (elt uri (+ i 1)) #\/)
			(equal (elt uri (+ i 2)) #\/)
			)
			(setq has-prt t)
	));)
	
	has-prt
)) ; }}}

;; }}}

;;; }}}


;; full basenames - name-page-str/lisp, page-file-p, split-pagename {{{

(defun name-page-str (id-no nickname filetype) ; string filename {{{
"construct string filename of a page given ID string and nickname"
	(let ((safe-id (format nil "~a" id-no))
		(ext (if (null filetype)
			""  (join-str "." filetype)
		))
	)
	
	(cond
	((equal safe-id "0") (join-str nickname ext))
	((null nickname) (join-str safe-id ext))
	(t (join-str safe-id "_" nickname ext))
))) ; }}}

(defun name-page-lisp (id-no nickname filetype) ; lisp {{{
"construct lisp pathname of a raw page given ID and nickname"
	(let ((safe-id (format nil "~a" id-no)))
	(if (null nickname)
		(make-pathname :name safe-id :type filetype)
		(make-pathname :name (join-str safe-id "_" nickname) :type filetype)
	)
)) ; }}}

(defun name-page (id-no nickname) ; name raw page file {{{
"construct string filename of a raw page file given ID and nickname"
	(let ((safe-id (format nil "~a" id-no)))
	(name-page-str safe-id nickname "txt")
)) ; }}}

(defun page-file-p (filename) ; determine if this is 'probably' a page filename {{{
	(let (frst has-idno (text-file t)
		(basename (pathname-name filename)) (ext (pathname-type filename))
		(media (list "png" "jpg"))
	)
	
	;; filename is 'probably' an ID if it starts with a digit or is 'root'
	(setq frst (elt basename 0))
	(setq has-idno
		(or (id-digit-p frst) (equal basename "root"))
	)
	;; if file is correctly named, check if it's Not a binary type
	(when (equal has-idno t)
		(setq text-file (null (string-member-p ext media)))
	)
	
	(and has-idno text-file)
)) ; }}}

(defun split-pagename (filename) "get ID and nickname from a page filename" ; {{{
	(let (full-path id-list nick-list in-label has-sep name)  ; id
	
	(setq full-path (uiop:ensure-pathname filename))
	
	;; separate name from type
	(setq filename (pathname-name full-path))
	
	;; isolate end label
	(loop for chr across filename do ; {{{
		;; if not already in end label
		(unless (equal in-label t)
			;; if char code is above 0 and below 9
			(if (and (char<= chr #\9) (char>= chr #\0))
				;; add char to ID string
				(setq id-list (cons (string chr) id-list))
				(setq in-label t)
		))
		;; if in end label
		(when (equal in-label t)
			(if (equal has-sep t)
				;; if past separator add to label list
				(setq nick-list (cons (string chr) nick-list))
				;; otherwise skip separator
				(setq has-sep t)
		))
	) ; }}}
	
	;; return result: (LIST id name filename)
	(let ((id (join-str (nreverse id-list)))) ; {{{
		(unless (null nick-list)
			(setq name (join-str (nreverse nick-list)))
		)
		
		;; if there is no ID number, return dummy data
		(when (equal id "")
			(setq id "0")
			(setq name filename)
		)
	
		(list id name filename)
	) ; }}}
)) ; }}}

;; }}}
