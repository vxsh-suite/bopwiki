;;; journal.lisp - journalling related functions
(defpackage :bopwiki-cmd-journal
	(:use :common-lisp :bopwiki-util :bopwiki-uri :bopwiki-filesystem
		:bopwiki-core :bopwiki-core-command
		:bopwiki-cmd-open
	)
	(:export
		#:today-page
		#:goto-bop
))
(in-package :bopwiki-cmd-journal)


;; private util {{{

(defun journal-date-offset () ; {{{
"Supply an offset in order to roll over journal dates before/after midnight"
18000  ;= (60 * 60) * 5 hours
) ; }}}

(defun journal-date(idno) ; date for new journal ex. 1-01 {{{
	(let ((raw-date idno) date month day (separator "-") offset)
	
	(setq offset (journal-date-offset))
	(unless (null offset)
		(setq raw-date (- idno offset))
	)
	
	(setq date (parse-month-day raw-date))
	(setq month (nth 0 date))
	(setq day (nth 1 date))
	
	(format nil "~d~a~2,'0d" month separator day)
)) ; }}}

;; }}}


(defun today-page(&key pwd wants action) "set/open journal entry for today" ; {{{
	(let ((page wants) filename name-info config-date  (write-config t))
	action  ; leave dummy alone
	
	(cond
	;; if not explicitly given a page, calculate whether to write config
	((null page) ; {{{
		(let (date title)
		;; get the date a current entry would be
		(setq date (get-unix-time))
		(setq title (journal-date date))
		
		;; try to read page from ./bop/today
		(setq filename (read-config-string "today" pwd))
		;; if stored filename was found, try to parse it 
		(unless (or (null filename) (equal filename ""))
			(setq name-info (nth 0 (split-pagename filename)))
			(setq config-date
				(journal-date (date-to-epoch name-info))
		))
		
		(cond
			;; if saved page name is today's date, open existing entry
			((equal config-date title) (setq write-config nil))
			;; if day has rolled over, create new entry
			;; BUG: respect filetype
			(t (setq filename (name-page-lisp date title "txt"))
		))
		
		;; edit the page
		(open-page :wants filename :pwd pwd)
		(setq filename (namestring filename))
	)) ; }}}
	(t (setq filename page)))
	
	;; BUG: only whole literal filenames work.
	;; allow looking up page by any valid identifier as in 'bop open'
	
	;; filename can be written if calculated, or explicitly specified
	(when (equal write-config t)
		(write-config-string "today" filename pwd))
)) ; }}}


(defun goto-bop(page pwd) ; 'command' to get bookmarked wiki dir - unused right now {{{
"try to go to a bookmarked microwiki directory (unfinished)"
	(let (saved-wikis lines wikipath  ; , pagefirst, targetstr, targetlen {{{
		;; get information about the wiki's name; info further down
	) ; }}}
	pwd ; leave dummy alone
	
	;; this feature more or less exists just in anticipation of the hypothetical GUI
	;; various shell / symlink strategies like pushd+popd may be a little better than it.
	
	(cond
	((not-null page)
	(let ((pagefirst (char page 0))
		(targetstr (join-str page " ")) (targetlen (+ (length page) 1))
	)
	
	;; get path to saved-wikis file
	;; it's ridiculously hard to merge pathnames without passing the subpath to this function?
	(setq saved-wikis (uiop:xdg-config-home "bopwiki" "saved-wikis"))
	;; open the file
	(setq lines (uiop:read-file-lines saved-wikis))
	
	;; loop through lines in saved-wikis file
	(dolist (line lines) ; {{{
		(let ((frst (char line 0)))
		;; if the first letter of the line & of the requested microwiki match,
		;; check if the whole wiki name string is found, and if so, continue
		(when (and (equal frst pagefirst) (equal targetstr (subseq line 0 targetlen)))
			;; when the wiki is found the loop will still continue
			;; as i think later duplicate entries overriding earlier ones is best
			(setq wikipath (subseq line targetlen))
		)
	)) ; }}}
	;; these manual string algorithms may be a bit silly
	;; but they make it easier to avoid dependencies
	
	;; i'm not sure how to make bop change to the folder, so this just prints it now
	;; i tried "pushd $(echo `bop goto asekai`)" but it isn't working
	;; LATER: actually change the directory
	(princ wikipath)
	
	))
	)
)) ; }}}


;;; register commands

;(register-command "today" 'today-page)
