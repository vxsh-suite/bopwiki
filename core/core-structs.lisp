(defpackage :bopwiki-core-structs
	(:use :common-lisp)
	(:export
		#:bop-page  #:make-bop-page
		#:bop-page-path #:bop-page-id #:bop-page-title #:bop-page-ext #:bop-page-body #:bop-page-meta #:bop-page-excerpt #:bop-page-tablerow
		
		#:bop-pageattr  #:make-bop-pageattr
		#:bop-pageattr-revision
		
		#:meta-line  #:make-meta-line #:meta-line-p
		#:meta-line-kind #:meta-line-kindsym #:meta-line-label #:meta-line-value #:meta-line-rel #:meta-line-num #:meta-line-raw
		
		#:meta-rule  #:make-meta-rule
		#:meta-rule-kind #:meta-rule-icon
))
(in-package :bopwiki-core-structs)


(defstruct bop-page  ; struct for a page read from filesystem
	path ext  id title  body meta excerpt
	tablerow  ; page metadata in the form of a bop-tablerow
)
(defstruct bop-pageattr
	;; built-in basic page categories
	;; LATER: outdated, replace with something less convoluted and more flexible
	revision
)

;; LATER: remove into wherever the rule parsing code will go? or maybe this really is where it should live
(defstruct meta-line
	kind  ; old hardcoded meta line kind string
	kindsym  ; TEMPORARY until all meta line related code uses symbols
		;; when everything is ported over, kindsym may simply reside in :kind
	
	rel  ; role/relation for lines like attachments
	value  ; internal value of meta line, such as entry number being referenced
	label  ; intended to be visible label of meta line
	num  ; letter/number code for referencing or including meta lines in body text
	
	raw  ; raw text of meta line, for view source in render-html, etc
)

;; LATER: is this unused??
(defstruct meta-rule  kind icon)
