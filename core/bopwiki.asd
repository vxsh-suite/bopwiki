(defsystem "bopwiki"
	:description "bop microwiki"
	:author "Valenoern"
	:licence "GPL"
	;; LATER: dynamically put the version into the .fasl while compiling the system. somehow.
	
	:depends-on (
		"uiop"
		"grevillea"  ; ver 2022-10-17+a or later - see library's commit log
	)
	:components (
		;;; components independent of / required by core
		(:file "util")  ; basic helper functions
		(:file "core-structs")
		(:file "page-uris"  :depends-on ("util"))
		(:file "filesystem"  :depends-on ("util" "page-uris"))
		
		;;; core - most essential parts of 'core' system
		(:file "core-table"  :depends-on ("util"))
		
		;; partly-finished experiment for new meta rules
		(:file "rules-parse")
		(:file "rules-line" :depends-on ("util" "core-table"))
		(:file "rules-body"  :depends-on ("util"))
		(:file "rules-render"  :depends-on ("rules-line"))  ; :bopwiki-linerules-render
		
		(:file "core-entryfile"  :depends-on ("core-table" "filters"))
		(:file "core-scanner")
		(:file "filters"  :depends-on ("util"))  ; LATER: ancient, revamp
		
		(:file "core"
			:depends-on ("util" "core-structs" "page-uris" "core-table" "core-entryfile" "core-scanner" "filesystem" "rules-line" "rules-body" "filters"))
		
		;;; elements atop core
		(:file "version" :depends-on ("core"))  ; LATER: ancient, make work again
		
		;; specific commands
		(:file "cmd"  ; :bopwiki-core-command - accept major subcommands
			:depends-on ("util" "core"))
		(:file "cmd-open"  :depends-on ("util" "page-uris" "cmd"))  ; bop new / open
		(:file "cmd-show"  :depends-on ("util" "filesystem" "cmd"))  ; bop list
		(:file "cmd-journal"  ; bop today / goto
			:depends-on ("util" "page-uris" "filesystem" "cmd-open"))
		
		;; package :bopwiki to export all symbols
		(:file "bopwiki-pkg"  :depends-on ("core"))
))


#| TODO left over from last update:
    * multiple values on time , tag ?
      * multiple value parser ?
      * multiple values for tag ?
      * check with example stems ? [note to self]

    * create alias with ::/time line
      * table: store page aliases with 'put-page-alias
      * linerules: store miscellaneous meta on :misclinks
      * bugfix: don't clear aliases on 'subset-tables, so aliased pages preview
    * multiple values on time , tag ?
      * accept :rel in ::/time line - ":: cr. 1667539161", etc.
      * multiple value parser ?
      * multiple values for tag ?
      * check with example stems ? [note to self]
    * use library "grevillea" for 'printl

|#
