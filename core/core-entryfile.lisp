;;; core-entryfile - for reading individual entries from the filesystem {{{
;;; see core-scanner for getting all files to populate the page table
(defpackage :bopwiki-core-entryfile
	(:use :common-lisp
		:bopwiki-core-structs :bopwiki-util :bopwiki-uri :bopwiki-core-table
		:bopwiki-filters
		;; bopwiki-core depends on this package
	)
	(:local-nicknames
		(:table :bopwiki-core-table)
		(:rules :bopwiki-linerules)
	)
	(:export
		#:parse-page-lines #:read-page-file
		#:page-body
))
(in-package :bopwiki-core-entryfile)
;;; }}}


(defun parse-page-lines (lines  &key path with-raw) ; parse "all" info from page lines {{{
"get full information from a page file"

	(let ((filename path) body ext meta-lines full-body tablerow)
	
	;; process file lines
	(let (line-info line-kind meta-over) ; {{{
		line-kind  ; dummy
		;; read lines in reverse order so metadata can be read first
		(setq lines (nreverse lines))
		(dolist (line lines) ; {{{
			;; when a meta line is found, extract info and add to meta
			(when (null meta-over)
				(setq line-info (rules:line-meta-info line))
				
				(cond
				;; if this is not a known meta kind possibly stop
				((null line-info)
					(cond
						;; if line blank skip it and continue
						((equal line "") '())
						;; if line non-blank end the meta section
						(t (setq meta-over t)))
				)
				(t
					;; include raw meta line if requested
					(unless (null with-raw)
						(setf (meta-line-raw line-info) line))
					
					;; file meta line into meta section
					(setq meta-lines (cons line-info meta-lines))
				))
			)
			
			;; if meta section has ended, add body line to body
			;; because cons goes backward, these are put down in *forward* order.
			(unless (null meta-over)
				(setq body (cons line body)))
		) ; }}}
	) ; }}}
	
	;; read filename info
	;; LATER: cleanup this + next part
	(unless (null filename)
		(setq ext (pathname-type filename))
	)
	
	(let ((page-info (split-pagename filename))  page-id  page-title)
	(setq
		page-id (parse-integer (nth 0 page-info))
		page-title (nth 1 page-info))
	
	;; create page 'record' to put in hashtable
	(setq  ; tablerow full-body {{{
		tablerow  (make-bop-tablerow
			:idno page-id  :date page-id
			:title page-title  :filename filename
			:linksfrom nil  :linksto nil
				;; this is handled in basic-context-filter [OLD]
			:attr  (make-bop-pageattr)
		)
		full-body  (make-bop-page
			:path filename  :ext ext  :id page-id  :title page-title
			:body body  :meta meta-lines
			:tablerow tablerow)
	) ;; }}}
	
	;; filters {{{
	
	;; add "additional filters to process entry"
	;; BUG: the filters system has been overhauled to complement bopwiki-linerules, so all of these old 'filters' are outdated (but still in use as you can see)
	(setq full-body (new-row-filters full-body))
	
	;; NEW RULES MECHANISM: run registered filters
	;; LATER: merge the old filters file with this
	(dolist (filter rules:*parse-filter-list*)
		(setq full-body (funcall filter full-body)))
	
	;; }}}
	
	full-body
))) ; }}}

(defun read-page-file (filename  &key with-raw) ; open page file by filename {{{
"get full information from a page file by filename"

	(let (lines full-body row)
	(cond
		;; if file exists, use uiop to get its contents as list
		((uiop:file-exists-p filename)
			(setq lines (uiop:read-file-lines filename)))
		(t
			;; if a bogus filename is given, return an empty page
			;; errors for a nonexistent page should be handled elsewhere
			(setq lines (list "[page not found]"))
	))
	
	(setq
		full-body (parse-page-lines lines  :path filename :with-raw with-raw)
		row (bop-page-tablerow full-body)
		)
	(when (page-exists-p nil :row row)
		(setf (bop-page-tablerow full-body) row))
	
	full-body
)) ; }}}

(defun page-body (pagename  &key with-raw) ; load body text given page name {{{
"load page body text given page name"

	(let ((row (table:page-row pagename)) cached-body cached-row path body)
	
	(setq body
		(cond
			;; if page body already stored in tables...
			((not-null (setq cached-body (table:get-page-body row)))
				cached-body)  ; return saved body
			;; otherwise load page file if possible
			((page-exists-p nil :row row)
				(setq path (bop-tablerow-filename row))
				(read-page-file path  :with-raw with-raw))
			;; or else return nothing
			(t nil)
			))
	
	;; if there is both a valid tablerow and a usable page, add existing row to page
	(when (and (not (null body)) (page-exists-p nil :row row))
		(setf (bop-page-tablerow body) row))
	
	body
)) ; }}}
