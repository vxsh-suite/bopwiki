;;; filesystem.lisp - load files from filesystem, etc. {{{
(defpackage :bopwiki-filesystem
	(:use :common-lisp :bopwiki-util)
	(:export
		#:pathname-inside #:surer-pathname
		;; bop-extension-p
		#:find-bop-dir #:load-init-file
		#:write-config-string #:read-config-string
))
(in-package :bopwiki-filesystem)
;;; }}}


;; LATER: is this function needed? i never know what already exists
(defun pathname-inside (parent dir) ; {{{
	(make-pathname :directory
		(reverse (cons dir
			(reverse (pathname-directory parent)
		)))
)) ; }}}

(defun surer-pathname (dir) ; super-ensure path when directory lacks slash {{{
"super-ensure pathname when directory may lack a slash, ex. when a foo.bop directory is mistaken for a file"
	(setq dir (uiop:ensure-pathname dir))
	(let ((name (pathname-name dir)) (ext (pathname-type dir)))

	(cond
		;; test if current path contains directories or files
		;; if it doesn't, assume it's a file and return it unchanged
		((null name) dir)
		;; otherwise see if it needs fixing
		(t (let (within dirname current)
			(setq within (pathname-directory dir))
			;; if directory name has an extension, add it to name
			(setq dirname (cond
				((stringp ext) (join-str name "." ext))
				(t name)
			))
			;; add directory name back to path
			(setq current (reverse (cons dirname (reverse within))))
			(make-pathname :directory current)
		))
))) ; }}}


;;; config strings & init.lisp {{{

(defun bop-extension-p (dir-name) ; check if directory ends in ".bop" {{{
	(let (len last-letters)
	
	(unless (or (null dir-name)
	(null (stringp dir-name))
	(< (setq len (length dir-name)) 4)
	)
		(setq last-letters
			(subseq dir-name (- len 4) len))
		
		(if (equal last-letters ".bop")
			t  nil)
))) ; }}}

(defun find-bop-dir (pwd) ; find top level of wiki folder {{{
	(let ((dir-list (pathname-directory pwd)) len found-in)
	(setq len (length dir-list))
	
	;; loop back until /, etc
	(loop for i  from len downto 1  when (null found-in)  do
		(let (
			(dir (make-pathname :directory (subseq dir-list 0 i)))
			(last-dir (nth (- i 1) dir-list))
		)
		
		;; if .bop directory was found, stop loop
		;; LATER: or root.anything , or if parent named "*.bop"
		(when (or
			(equal (bop-extension-p last-dir) t)
			(uiop:directory-exists-p (pathname-inside dir ".bop"))
			)
			(setq found-in dir)
	)))
	found-in
)) ; }}}

(defun load-init-file (pwd) ; load config file {{{
"Loads and runs .bop/init.lisp for this particular wiki folder.
This allows customising bop's behaviour in various ways - see issues.bop/1620354797 in the manual for more info"
	(let (dir top hidden-dir init)
	
	(setq dir (surer-pathname pwd))
	(setq top (find-bop-dir dir))
	
	;; if top directory wasn't found, quit
	;; LATER: simply store path of 'init.lisp' when finding known pages
	(cond
	((null top) (format t "No init file found~%"))
	(t
	(setq hidden-dir (pathname-inside top ".bop"))
	
	;; assume for now that configuration can only be in init.lisp
	(setq init (merge-pathnames hidden-dir (make-pathname
		:directory (pathname-directory hidden-dir)
		:name "init" :type "lisp"
	)))
	
	(when (uiop:file-exists-p init)
		(load init)
		(format t "Loaded init file ~a~%" init)
    )
    )
))) ; }}}

(defun write-config-string (filename value pwd) ; write config to text file {{{
	"write a configuration string to a text file"
	(let (info-file)
	
	;; uiop will try to open ./bop/filename
	(setq info-file (join-str pwd "/.bop/" filename))
	
	;; to force overwriting file, delete file if exists
	(uiop:delete-file-if-exists info-file)
	
	;; try to write config string
	;; if an empty value was passed, its file will simply not exist now
	(unless (null value)
	(with-open-file (stream info-file :direction :output)
		(format stream value)
	))
)) ; }}}

;; LATER: this function + the above is outdated. replace it with the bop-render AP one when done
(defun read-config-string (filename pwd) ; open configuration value in .bop/ {{{
"open a configuration string from a text file"
	(let (info-file lines)
	
	;; uiop will try to open ./bop/filename
	;; BUG: does uiop have a better way to join the path name?
	(setq info-file (join-str pwd "/.bop/" filename))
	
	;; if file exists, read its text; otherwise return nil
	(when (uiop:file-exists-p info-file)
		(setq lines (uiop:read-file-lines info-file))
		;; assume the value is only one line long and return value
		(nth 0 lines)
))) ; }}}

;;; }}}
