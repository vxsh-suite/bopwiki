;;; body formatting rules
(defpackage :bopwiki-oldrules-formatting
	(:use :common-lisp :bopwiki-util)
	(:export
		#:formatting-result #:formatting-result-kind #:formatting-result-indented #:formatting-result-payload
		#:parse-line-kind
))
(in-package :bopwiki-oldrules-formatting)


;; rule data to be used by line parser
(defstruct formatting-rule ; {{{
	kind render-kind  is-block  test-function extract-function block-function
) ; }}}
(defstruct formatting-result ; {{{
	kind  ; name of rule?/name of formatting?
	indented  ; whether this line is part of (i.e. 'indented' by) a block rule
	payload  ; extracted text from inside a formatting tag
) ; }}}


;; generic body rule guts {{{

(defun extract-whole-line(line) line)
(defun extract-nothing(line) ; {{{
	line  ; so sbcl doesn't complain line is unused
	nil
) ; }}}

(defun extract-leading-formatting(line start) ; extract list bullet etc {{{
	(let ((len (length line)))

	(when (< start len)
		(string-left-trim " "  (subseq line start len))
	)
)) ; }}}

(defun rule-on-string(kind)
	(format nil "~a-on" kind)
)
(defun rule-off-string(kind)
	(format nil "~a-off" kind)
)

;; }}}


;; rule list {{{

(defun base-formatting-rules() ; {{{
	(list
		(make-formatting-rule  :kind "uni-hr"  :render-kind "hr"  :is-block nil
			:test-function 'divider-line-p
			:extract-function 'extract-whole-line
		)
		(make-formatting-rule  :kind "md-h2"  :render-kind "h2"  :is-block nil
			:test-function 'heading2-line-p
			:extract-function 'extract-heading-line
		)
		(make-formatting-rule  :kind "h1"  :render-kind "h1"  :is-block nil
			:test-function 'heading-line-p
			:extract-function 'extract-heading-line
		)
		(make-formatting-rule  :kind "md-li-x"  :render-kind "li-x"  :is-block nil
			:test-function 'checked-line-p
			:extract-function 'extract-checked-line
		)
		(make-formatting-rule  :kind "md-li"  :render-kind "li"  :is-block nil
			:test-function 'bullet-line-p
			:extract-function 'extract-bullet-line
		)
		(make-formatting-rule  :kind "uni-quote"
			:render-kind "blockquote"  :is-block t
			:test-function 'blockquote-line-p
			:extract-function 'extract-nothing
			:block-function 'toggle-blockrule-p
		)
		(make-formatting-rule  :kind "md-pre"  :render-kind "pre"  :is-block t
			:test-function 'pre-line-p
			:extract-function 'extract-nothing
			:block-function 'toggle-blockrule-p
		)
	)
) ; }}}

(defun all-formatting-rules()  (base-formatting-rules))

(defun lookup-rules(rule-list) ; get rules to test {{{
	(let ((rules (all-formatting-rules)) result)
	
	(dolist (rule rules)
		(when (string-member-p (formatting-rule-kind rule) rule-list)
			(setq result (cons rule result))
	))
	(nreverse result)
)) ; }}}

;; }}}


;;; rule guts {{{
;; LATER: modularise whole rules into functions or something
;; ideally, these names shouldn't leave the module & should just export as lambdas in the rules

;; BUG: not working
;; non-parsed line {{{
(defun escaped-line-p (line)
	(let ((len (length line)))
	
	(and
		(> len 2)
		(equal (elt line 0) #\\)
		(or
			(equal (elt line 1) #\#)
	))
))

(defun extract-escaped-line(line)
	(extract-leading-formatting line 1)
)
;; }}}

;; li - bullet line {{{
(defun bullet-line-p(line)
	(let ((len (length line)))
	
	(and
		(> len 0)
		(equal (elt line 0) #\*)
	)
))

(defun checked-line-p(line) ; * X
	(let ((len (length line)) inner)
	
	(when (and
		(> len 2)  (equal (elt line 0) #\*)
		)
		(setq inner (extract-leading-formatting line 1))
		(equal (elt inner 0) #\X)
	)
))

(defun extract-checked-line(line)
	(let (inner)
	(setq inner (extract-leading-formatting line 1))
	(extract-leading-formatting inner 1)
))

(defun extract-bullet-line(line)  (extract-leading-formatting line 1))
;; }}}

;; h1 , h2 , h3 - heading line {{{
(defun heading-line-p(line)
	(let ((len (length line)))
	(and
		(> len 2)
		(equal (elt line 0) #\#)
		(not (equal (elt line 1) #\#))
)))

(defun heading2-line-p(line)
	(let ((len (length line)))
	(and
		(> len 3)
		(equal (elt line 0) #\#)
		(equal (elt line 1) #\#)
		(not (equal (elt line 2) #\#))
)))

(defun extract-heading-line(line)
	(let ((len (length line)) (smallest-level 4) done)
	;; BUG: line shouldn't be detected as heading if not long enough
    (when (> len 0)
	(loop for i  from 0 to smallest-level  when (null done)  do
		(when (not (equal (elt line i) #\#))
			(setq done i)
	))
	(extract-leading-formatting line done)
	)
))
;; }}}

;; quote / pre lines {{{

(defun pre-line-p(line) ; {{{
	(let ((len (length line)) frst)
	(when (> len 2)
		(setq frst (subseq line 0 3))
		(equal frst "```")
))) ; }}}

(defun blockquote-line-p(line) ; quote lines {{{
	(let ((len (length line)) (frst (elt line 0)))
	
	(and
		(< len 3)
		(or
			(char= frst #\")
			(char= frst #\HEAVY_DOUBLE_TURNED_COMMA_QUOTATION_MARK_ORNAMENT)
			(char= frst #\HEAVY_DOUBLE_COMMA_QUOTATION_MARK_ORNAMENT)
	))
)) ; }}}

;; }}}

;; misc - hr {{{
(defun divider-line-p(line) ; ─, or multiple
	(let (not-dash)
	
	(loop for  chr across line  when (null not-dash)  do
		(unless (equal chr #\─)
			(setq not-dash t)
	))
	(not not-dash)
))
;; }}}

;;; }}}


;;; parse body lines {{{

(defun toggle-blockrule (in-block current-rule) ; {{{
"given whether a line has MATCHED-RULE (T/NIL) of the formatting rule kind IN-BLOCK (string/NIL), return T if a generic block rule is on or NIL if not"
	;(let (dummy)
	(cond
		((and (null in-block) (not-null current-rule))  current-rule)
		((and (not-null in-block) (equal in-block current-rule))  nil)
		((not-null in-block) in-block)
		(t nil)
));) ; }}}

(defun parse-line-kind(line in-block &key enabled) ; determine formatting {{{
	(let ((len (length line)) rules result indented (payload line))
	(when (> len 0)
		(setq rules (lookup-rules enabled)))
	
	;; find matching rule, if there is one
	(loop for  rule in rules  when (null result)  do
		(let (
			(test-p (formatting-rule-test-function rule))
			(kind (formatting-rule-render-kind rule))
		)
		;; when rule's test-function matches line, return rule and stop loop
		(when (funcall test-p line)
			(setq result kind)
			;; if there was a matching rule, extract inner part of line
			(setq payload (funcall (formatting-rule-extract-function rule) line))
		)
		;; but before stopping, toggle block if either in-block or this rule matches it
		(when (and  (or (equal kind result) (equal kind in-block))
			(equal (formatting-rule-is-block rule) t))
				(setq indented (toggle-blockrule in-block result))
				;; if block was changed, add 'on/off'
				(setq result
				(if (equal in-block indented)  in-block
					(cond
					((equal in-block nil)  (rule-on-string result))
					((equal indented nil)  (rule-off-string result))
					(t result)
				))
				)
				;(rintl "PARSE -" "IN-BLOCK" in-block "AND MATCHING" result "->" indented)
	)))
	
	(make-formatting-result  :kind result  :indented indented  :payload payload)
)) ; }}}

;;; }}}
