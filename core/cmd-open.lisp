;;; open.lisp - open and edit a new or existing page {{{
(defpackage :bopwiki-cmd-open
	(:use :common-lisp :bopwiki-core-structs :bopwiki-util :bopwiki-uri
		:bopwiki-core-table :bopwiki-filesystem :bopwiki-core :bopwiki-core-command)
	(:local-nicknames (:table :bopwiki-core-table))
	(:export
		#:*editor-command* ;#:*editor-terminal*
		
		;; append-to-command
		;; edit-page
		
		#:new-page-title #:new-page
		#:open-page
		#:renamed-page-title #:rename-page
))
(in-package :bopwiki-cmd-open)


(defparameter *editor-command* (list "kate"))
(defparameter *editor-terminal* nil)

;; examples tested:
;; (list "emacsclient" "-n")  ; open in running/server emacs
;; (list "kate")  ; simple graphical editor, same for geany/mousepad/etc

;; LATER:
;; - I absolutely can't get terminal editors like nano to work yet after trying a bunch of things
;; - xdg-open will not open a non-existent file... so i am back to kate for no good reason

;;; }}}


;;; bop new , bop open {{{

;; edit-page {{{

(defun append-to-command (command path-string)
	(reverse (cons path-string (reverse command)))
)

(defun edit-page (page-file)
"open a page filename in external editor"
	(let (cmd
		(base-command 
			(append-to-command *editor-command*
				(namestring page-file)))
	)
	
	(cond
		((null *editor-terminal*)
			(setq cmd base-command)
			(uiop:launch-program cmd)
		)
		#|(t
			;;(setq cmd
				;;(append-to-command *editor-terminal* (join-str-with base-command " ")))
			(setq cmd
				(list "nano" "foo.txt")
			)
			(uiop:run-program cmd  :force-shell t  :output :interactive)
		)|#
	)
))

;; }}}

(defun new-page-title (wants) ; title for new page {{{
	;; LATER: allow other default filetype
	(let ((filetype "txt"))
	(cond
		;; if a valid iso/epoch date is supplied, make page using that ID
		((is-valid-date wants)
			(name-page-lisp (date-to-epoch wants) nil filetype))
		;; if a valid nickname is supplied, make page with that nickname
		((not (null wants))
			(name-page-lisp (get-unix-time) wants filetype))
		;; otherwise simply make a page with the current time
		(t (name-page-lisp (get-unix-time) nil filetype))
	
	;; LATER:
	;; - a small epoch time should probably be zerofilled
	;; - when 'newbird' is finished, render filename in appropriate format
))) ; }}}

(defun new-page (&key pwd wants action) ; "bop new" command {{{
"open file representing new wiki page"
;; CLI: bop new [wants]
	(let ((newpage (new-page-title wants)) page-path)
	(setq page-path (merge-pathnames (surer-pathname pwd) newpage))
	action  ; leave dummy alone
	
	(edit-page page-path)
)) ; }}}

(defun open-page (&key pwd wants action) ; "bop open" command {{{
"try to open page in editor given ID or name"
;; CLI: bop open [wants]
	(let ((arg wants) to-open)
	action  ; leave dummy alone
	
	;; page name may be passed as pathname from bop today
	(cond
		((stringp arg)
			;; get known pages and try to find page
			(find-all-pages pwd)
			(setq to-open (table:page-filename arg))
		)
		(t (setq to-open arg))
	)
	
	(cond
	;; if page exists, flatten it to a namestring and open it
	((not (null to-open))
		(setq to-open (namestring to-open))
		(edit-page to-open)
	)
	;; otherwise try to create a new page
	(t (new-page :wants arg :pwd pwd))
))) ; }}}

;;; }}}


(defun renamed-page-title (wants nickname) ; "bop rename" filename {{{
	;; mainly for testing, a page table can be passed in
	(let (row filename)
	;; try to lookup page + its id number
	(setq row (table:page-row wants))
	(setq filename (bop-tablerow-filename row))
	
	(unless (or (null row) (null filename))
		(let ((new-id (bop-tablerow-idno row)) ext) ; within new-name
		
		;; get file extension
		(setq ext (pathname-type filename))
		
		;; if an iso date is passed, convert it to an epoch date
		;; LATER: use date-to-epoch? + create test
		(when (is-iso-date nickname)
			(setq nickname (iso-to-epoch nickname))
		)
		;; if nickname is now an epoch date
		(when (is-epoch nickname)
			;; use as id number, and fetch existing nickname
			(setq new-id (format nil "~a" nickname))
			(setq nickname (bop-tablerow-title row))
		)
		
		;; make new page filename from id number and new nickname
		(name-page-lisp new-id nickname ext)
	))
)) ; }}}

(defun rename-page (&key pwd wants action) ; "bop rename" {{{
"change the nickname of a page on filesystem"
;; CLI: bop rename X Y
	;; scan for all known pages
	(find-all-pages pwd)
	(let ((nickname action) filename)
	(setq filename (table:page-filename wants))
	
	(unless (null filename)
		(let (within new-name new-path)
		
		;; get directory file is in, + its file extension
		(setq within (uiop:pathname-directory-pathname filename))
		;; find name page will be renamed to
		(setq new-name  (renamed-page-title wants nickname))
		
		;; make new filename 'probably' absolute
		(setq new-path (merge-pathnames within new-name))
		
		;; rename page on filesystem
		;; LATER: more safeguards?
		(uiop:rename-file-overwriting-target filename new-path)
		
		;; return new filename
		new-name
	))
	
	;; BUG: this currently only works from the top level folder
)) ; }}}


;;; register commands

(register-command "new" 'new-page)
(register-command "open" 'open-page)
(register-command "rename" 'rename-page)
