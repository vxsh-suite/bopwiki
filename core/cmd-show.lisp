;;; show.lisp - print information about known entries {{{
(defpackage :bopwiki-cmd-show
	(:use :common-lisp :bopwiki-util
		:bopwiki-core-table :bopwiki-core
		:bopwiki-filesystem :bopwiki-core-command
	)
	(:local-nicknames (:table :bopwiki-core-table))
	(:export
		#:list-pages-item #:list-pages-table #:list-pages
))
(in-package :bopwiki-cmd-show)
;;; }}}


;;; "bop list"

(defun list-pages-item (page) ; single entry for "bop list" {{{
	(let ((id (table:page-id page))  (title (table:page-label page)) safe-id pretty-id)
	
	(setq safe-id (table:justify-idno id))
	(setq pretty-id (color-text "green" safe-id))
	
	(when (null title) (setq title ""))
	
	;; list page
	(format nil "~a ~a" pretty-id title)
)) ; }}}

(defun list-pages-table (which-pages) ; all entries for "bop list" {{{
	;; for each page
	(dolist (page which-pages)
		;; list page
		(princ (list-pages-item page)) (terpri)
	)
	;; this is its own function almost solely for "bop/test" purposes.
) ; }}}

(defun list-pages (&key pwd wants action) "list all unique pages" ; "bop list" {{{
	(let ((dir (surer-pathname pwd)) which-pages)
	wants action  ; leave dummy alone
	
	;; fetch all pages which aren't revisions
	(find-all-pages dir)
	(setq which-pages (table:non-revision-pages))
	
	(list-pages-table which-pages)
)) ; }}}


;;; register commands

(register-command "list" 'list-pages)
