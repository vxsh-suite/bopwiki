(defpackage :bopwiki-core-command
	(:use :common-lisp :bopwiki-util)
	(:local-nicknames
		(:bop :bopwiki-core)
	)
	(:export
		#:*command-list* #:register-command
		#:do-command
))
(in-package :bopwiki-core-command)


(defparameter *command-list* (make-hash-table :test 'equal))

(defun register-command (command-string command) ; {{{
	(puthash command-string command *command-list*)
) ; }}}

(defun do-command (command-string  &key pwd wants action) ; {{{
	(let ((command
		(gethash command-string *command-list*)))
	
	(when (null pwd)
		(setq pwd bop:*top-directory*))
	
	;; run command if found
	(if (null command)
		(format t "Unknown command '~a'" command-string)
		(funcall command :pwd pwd :wants wants :action action)
	)
	;; in order to be passed to do-command, a command needs the following &key args:
	;;   &key pwd wants action
	;; LATER: maybe make this a struct to prevent commands from breaking in the future as additional args are added
)) ; }}}
