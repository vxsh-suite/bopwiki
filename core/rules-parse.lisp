;;; rules-parse.lisp {{{
(defpackage :bopwiki-linerules-parse
	(:use :common-lisp :bopwiki-core-structs :bopwiki-util
		:bopwiki-uri :bopwiki-core-table
	)
	(:local-nicknames
		(:table :bopwiki-core-table)
	)
	(:export
		#:split-value-at
		
		#:rule-parser-value
		#:rule-parser-label
		#:rule-parser-comment-role #:rule-parser-role-comment
		#:rule-parser-label-comment
))
(in-package :bopwiki-linerules-parse)
;;; }}}


(defun rule-parser-value (line min-len) ; extract value from text meta line {{{
"Extract meta line value from raw meta line string LINE given icon length MIN-LEN"
	(let ((len (length line)) value)
	
	;; if there is a character after meta line icon
	(when (>= (+ len 1) min-len)
		(setq
			;; try to get line data (value)
			value (subseq line min-len)
			;; trim any spaces between meta line symbol & value
			value (string-left-trim " " value)
	))
	value  ; return value
)) ; }}}

(defun split-value-at (value chr-literal) ; {{{
;; split off a second value after value if applicable
	(let (line-split beginning end)
	
	;; get position where split character was found
	(setq line-split
		(loop for pos from 0 to (- (length value) 1)
			when (equal (elt value pos) chr-literal)
			return pos))
	
	(unless (null line-split)
		(setq
			beginning
				(string-trim " "
					(subseq value 0 (+ line-split 0)))
			end
				(string-trim " "
					(subseq value (+ line-split 1)))
		))
	
	;; return multiple values
	(if (null beginning)
		(values value nil)
		(values beginning end))
)) ; }}}

(defun rule-parser-label (line) ; get label for cxt, fwd, obj, ver? lines {{{
	(multiple-value-bind (value comment)
		(split-value-at (meta-line-value line) #\ )
		comment  ; discard comment
		
		(setf (meta-line-value line) value)
	)
	line
) ; }}}

(defun rule-parser-comment-role (line) ; get role for excerpt line {{{
	(multiple-value-bind (value rel)
		(split-value-at (meta-line-value line) #\ )
		
		(setf (meta-line-value line) value)
		(setf (meta-line-rel line) rel)
		)
	line
) ; }}}

(defun rule-parser-role-comment (line) ; get role for excerpt line {{{
	(multiple-value-bind (rel value)
		(split-value-at (meta-line-value line) #\ )
		
		;; if only given one value, assume the first is VALUE
		(when (null value)
			(setq
				value rel
				rel nil))
		
		(setf (meta-line-value line) value)
		(setf (meta-line-rel line) rel)
		)
	line
) ; }}}

(defun rule-parser-label-comment (line) ; {{{
	(multiple-value-bind (value big-label)
		(split-value-at (meta-line-value line) #\ )
		
		(multiple-value-bind (label comment)
			(split-value-at big-label #\;)
				comment  ; discard comment
				
				;; do not actually try to validate meta line value
				
				(setf (meta-line-value line) value)
				
				;; treat "label" as comment if no comment, otherwise use it
				(unless (null comment)
					(setf (meta-line-label line) label))
	))
	line
) ; }}}

(defun rule-parser-multiple (line) ; accept multiple meta lines separated by commas {{{


	(multiple-value-bind (value remainder)
		(split-value-at (meta-line-value line) #\,)
		
	)
	
	line
	
	;; return list of 'meta-line structs
) ; }}}
