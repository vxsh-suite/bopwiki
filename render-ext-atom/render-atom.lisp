;;; render-gmi.lisp - atom feed renderer {{{
(defpackage :bop-render-atom
	(:use :common-lisp :bopwiki :bop-render-core :bop-render-targets
		:bop-render-atom-page
	)
	(:local-nicknames
		(:table :bopwiki) (:render :bop-render-core)
		(:html :bop-render-html-2)
		(:meta :bop-render-atom-author)
		;; uiop
	)
	(:export
		#:publish-atom-path #:*publish-atom-path*
		;; atom-template
		;; render-feed-atom
		#:publish-bop-atom
))
(in-package :bop-render-atom)
;;; }}}


;;; config functions - publish-atom-path {{{

(defparameter *publish-atom-path* nil) ; {{

(defun publish-atom-path (path)
"Set directory to publish rendered atom feed of entries to when running 'bop publish', using the lisp pathname PATH.
This function is designed to be used in init.lisp, and to nest any arbitrary function to calculate the pathname, ex. (namestring \"/path/to/output\")"
	(let ((path-object (uiop:ensure-pathname path)))
	;; BUG: check?
	(setq *publish-atom-path* path-object)
))
;; }}

;;; }}}


(defun atom-template (meta entries) ; whole feed xml string {{{
"Renders an atom feed file as a string based on supplied properties.
All arguments should be strings and ENTRIES should contain all feed entries as a single string"

	(let ((meta-str (meta:atom-meta-template meta)))
	(format nil 
"<?xml version=\"1.0\" encoding=\"utf-8\"?>
<feed xmlns=\"http://www.w3.org/2005/Atom\">
~a~a

</feed>"
		meta-str entries
		;; "entries" prints right next to "updated" because all newlines are above each entry
))) ; }}}


(defun render-feed-atom (page-list  &key bop-version) ; {{{
	(let ((version bop-version) entries feed-info (update-date 0))
	
	;; create metadata struct to pass to entries and templating
	(setq feed-info (meta:atom-feed-meta :bop-version version))
	
	;; render individual pages, + find updated date
	(dolist (pagename page-list) ; {{{
		;; find updated date of feed based on 
		(let ((date
			(bop-tablerow-date (table:page-row pagename))
		))
		(when (> date update-date)  (setq update-date date))
		
		;; render entry
		(setq entries (cons
			(render-entry-atom pagename :feed-meta feed-info)
			entries)
		)
	)) ; }}}
	
	;; BUG: updated date should be date of latest entry, but doesn't seem to be
	
	;; render feed to string
	(atom-template feed-info (join-str entries))
)) ; }}}

(defun publish-bop-atom (target cwd) ; {{{
	(let (output page-list feed-name feed)
	target cwd output page-list  ; leave dummy alone
	
	;; if no publish path, use the bop-render one
	;; LATER: implement in other formats
	(when (null *publish-atom-path*)
		(publish-atom-path render:*default-publish-path*)
	)
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname *publish-atom-path*))
	(ensure-directories-exist output)
	
	;; publish formats now take a standardised 'publish target' struct
	(setq page-list (publish-target-pages target))
	
	;; render individual pages
	(setq
		feed-name (make-pathname :name "atom" :type "xml")
		feed (render-feed-atom page-list)
	)
	(export-in-dir output feed-name feed)
	
	nil
)) ; }}}
