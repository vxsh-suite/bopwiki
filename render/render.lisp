;;; containing package for bop-render

(uiop:define-package :bop-render
	;(:use :common-lisp :bopwiki)
	(:mix :common-lisp :bopwiki)
	(:use-reexport :bop-render-core :bop-render-targets
		;; :bop-render-html :bop-render-atom
	)
)

;; LATER: try separating out private structs to only :use
