(defpackage :bop-render-author  ; info for atom feed author / AP actor / etc {{{
	(:use :common-lisp :bopwiki :bop-render-core) ;:uiop
	;; depends on systems: uiop  cl-rdfxml puri "conversion-thingy"
	(:local-nicknames
		(:bop :bopwiki) (:table :bopwiki)
	)
	(:export
		#:test
))

;; this package is currently required for bop-render with the thinking that probably, most publish formats are more likely to need it than not need it.
;; this may change, given a bop-rdf dependency is a little hefty just to generate html.
;; the most likely place for this package to move is straight into bop-rdf so you get both at once.

(in-package :bop-render-author)
;;; }}}


(defun find-actor-schema () ; {{{
	(let ((path (uiop:ensure-pathname "/home/takumi/git-repos/owl-wings/wing-bop/bop.xml"))
		input triple-table dict
	)
	
	;; load xml schema
	(setq input (uiop:read-file-string path))
	;; make a table that for now defines the b:cite element
	(setq triple-table (conversion-thing-table:xml-to-table input))
	(setq dict (conversion-thing-dict:table-to-dict triple-table))
	
	dict  ; return dictionary
)) ; }}}

(defun extract-rdf-item (triple-table) ; reduce table to one item {{{
	(let (result)
	
	(maphash #'(lambda (k v)
		k  ; leave dummy alone
		(setq result v)
	) triple-table)
	
	result
)) ; }}}

(defun typed-rdf-item (triple-table schema) ; {{{
	(let ((rdf-item (extract-rdf-item triple-table))) ; actor
	
	(maphash #'(lambda (k v)
		(let ((prop (gethash k schema)) datatype (converted v))
		(unless (null prop)
			(setq datatype (conversion-thing-dict:property-datatype prop))
		)
		(unless (null datatype)
			(cond
				((puri:uri= datatype (puri:uri "http://www.w3.org/2001/XMLSchema#anyURI"))
					(setq converted (puri:uri v)))
				((puri:uri= datatype (puri:uri "http://www.w3.org/2001/XMLSchema#dateTime"))
					(setq converted (iso-to-epoch v)))
				;; any of the string types are left alone
				;; LATER: if necessary, have a way to synonymise type URIs
			)
			(puthash k converted rdf-item)  ; put processed value back into table
		)
	)) rdf-item)
	
	rdf-item
)) ; }}}

(defun test ()
	;; attempt to get actor information from page table
	;; currently it must be in xml format. ideally .ttl will work eventually
	(let (actor-data schema input actor triple-table) ;path
	
	;; get schema describing actor format
	(setq schema (find-actor-schema))
	
	;; get actor data file from page table
	(setq actor-data (bop:page-body "actor"))
	
	;; try to load actor data if it exists
	(unless (null actor-data)
		(setq input (bop-page-body actor-data))
		;; if necessary, convert input to string
		(when (listp input)
			(setq input (bop:join-lines input)))
		
		;; make a table that for now represents the b:cite element
		(setq triple-table (conversion-thing-table:xml-to-table input))
		;; get single item with types
		(setq actor (typed-rdf-item triple-table schema))
	)
	
	actor
	
	;; LATER: bop-rdf & the bop author/actor schema are in early stages. this version of this package may not even work out of the box in the future should they update.
	;; this will all be made less wonky later.
))

;; BUG: subset-tables is not automatically including actor.
;; it should have been enough to attach it to the root node but it isn't?


#| an actor file looks something like:

<?xml version="1.0" encoding="UTF-8" ?>
<rdf:RDF
        xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
        xmlns:b="http://distributary.network/owl-wing/2021-06/bop#">

<b:cite rdf:ID="actor-at-example-cite">
        <b:created>2019-07-31T11:49:14Z</b:created>
        <b:username>actor</b:username>
        <b:host>example.com</b:host>
        <b:title>actor@example.com</b:title>
        <b:displayName>actor</b:displayName>

        <b:keyPathPublic>~/.ssh/actor.pem</b:keyPathPublic>
        <b:keyPathPrivate>~/.ssh/actor-pub.pem</b:keyPathPrivate>
</b:cite>

</rdf:RDF>
|#
