(defsystem "bop-render"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "'bop publish' core"
	
	:depends-on ("uiop" "bopwiki"  "cl-rdfxml" "puri" "owl-wings")
	:components (
		(:file "render-core-targets")  ; publish targets
		
		(:file "uri")  ; :bop-render-uri
		(:file "render-core" :depends-on ("render-core-targets"))
		(:file "render-rules")  ; :bop-render-rules
		
		(:file "render-author"  ; :bop-render-author
			; may move to another system eventually. see inside
		)
		
		;; :bop-render package, the one that re-exports everything in this system
		(:file "render" :depends-on ("render-core" "render-core-targets"))
		
		;; render-html, -gmi, -atom - see bop-render- systems
))
