;;; render.lisp - core for publish formats {{{
(defpackage :bop-render-core
	(:use :common-lisp
		:bopwiki
			;; join-lines
		:bop-render-targets
	)
	(:local-nicknames
		(:table :bopwiki) (:rules :bopwiki)
		(:meta :bop-rules-meta)
		(:targets :bop-render-targets)
		(:uri :bop-render-uri)
	)
	(:export
		;; structs - not supposed to be exported to bop-render
		;; LATER: see if :use in define-package can fix this
		#:make-rendered-entry #:rendered-entry-id #:rendered-entry-body #:rendered-entry-date #:rendered-entry-metablock #:rendered-entry-summarised #:rendered-entry-raw #:rendered-entry-isreply #:rendered-entry-nesting
		
		;; half baked display functions
		#:line-is-empty #:add-header-line
		#:page-date-placeholder
		
		;; baked config functions
		#:default-publish-format #:*default-publish-format*
		#:post-cutoff-limit #:post-excerpt-limit #:*post-cutoff-limit* #:*post-excerpt-limit*
		#:default-publish-path #:*default-publish-path*
		#:discard-previews #:*discard-previews*
		#:entry-pronounce-function #:*entry-visual-pronounce-function* #:*entry-aural-pronounce-function*
		
		;; things
		#:process-entry-body
		#:reconstruct-raw-page  ; #::reconstruct-meta-line
		
		;; manual summaries
		#:excerpt-visible-outlinks
		#:excerpt-text #:will-excerpt-p
		
		;; exporting
		#:export-page #:export-in-dir
		#:entry-web-url #:exported-page-url
		
		;; publish helpers
		#:parse-wants #:export-announce
		
		;; command
		#:render-filters
		#:publish-bop
		
		#:pronounce-entry-id
))
(in-package :bop-render-core)


(defstruct rendered-entry  ; rendered entry abstraction {{{
	;; to abstract passing rendered entries to the templating function,
	;; so it doesn't need a ridiculous number of elements.
	id header body date metablock summarised  raw isreply nesting
) ; }}}

;; LATER: globally cache pages?
;(defvar *page-cache*)

;;; }}}


;;; improved config functions {{{

(defparameter *post-cutoff-limit* 600) (defparameter *post-excerpt-limit* 500) ; {{{

(defun post-cutoff-limit (limit)
	(when (numberp limit)
		(setq *post-cutoff-limit* limit)
))
(defun post-excerpt-limit (limit)
	(when (numberp limit)
		(setq *post-excerpt-limit* limit)
))

;; }}}

(defvar *default-publish-format*) ; {{{
;; it's only possible to set this to 'publish-bop-html in e.g. render-html.lisp
(defun default-publish-format (format-name)
	(setq *default-publish-format* format-name))
;; TEMPORARY.
;; LATER: move to a system more like the register-command system
;; }}}

;; default place to put rendered versions of entries
;; in general, render formats should fall back to this if more specific settings are empty
(defparameter *default-publish-path* (parse-namestring "/tmp/bop/")) ; {{{

(defun default-publish-path (path)
	(setq *default-publish-path* path))
;; }}}

;; whether to apply "no-replies" to all entries - T or NIL
(defparameter *discard-previews* nil)
(defun discard-previews (&optional (whether nil whether-suppliedp))
	;; if 'whether is completely absent, assume T,
	;;   but if it was somehow present and unset, assume NIL
	(when (null whether-suppliedp)  (setq whether t))
	;; set *discard-previews*
	(setf *discard-previews* whether))

(defparameter *entry-aural-pronounce-function* nil) ; {{{
(defparameter *entry-visual-pronounce-function* nil)

(defun entry-pronounce-function (func  &key aural both) ; {{{
	(let (
		(func-to-set
			(if (null func)
				(lambda (epoch  &key words)
					(declare (ignore words))
					(format nil "~a" epoch))
				func))
	)
	
	;; set function if told to by AURAL, or if BOTH is set
	(when (or (not-null aural) (not-null both))
		(setq *entry-aural-pronounce-function* func-to-set)
	)
	(when (or (null aural) (not-null both))
		(setq *entry-visual-pronounce-function* func-to-set))
)) ; }}}
(entry-pronounce-function nil :both t)
;; }}}

;;; }}}

(defun pronounce-entry-id (epoch  &key visual words) ; {{{
	(let (;pronunciation raw
		(bluebird-function
			(cond
				;;((null raw)  *entry-pronounce-function*)
				((null visual)  *entry-aural-pronounce-function*)
				(t  *entry-visual-pronounce-function*)))
	)
	
	(funcall bluebird-function epoch :words words)
)) ; }}}

;;; page body functions {{{

(defun page-date-placeholder (date) ; footer date placeholder on root {{{
"placeholder for exported entries without date such as root"
	;; LATER: localise no date
	
	(if (equal date 0)
		"[no date]"
		nil)
) ; }}}

;;; }}}

;;; display portion {{{
;; LATER: wherever you see them in bop code, 'display functions' are outdated and set to be removed eventually. they were made at a time where i thought you could redefine functions in init.lisp but that is not so viable

(defun line-is-empty (line) ; {{{
	(cond
		((equal line "") t)
		(t nil)
)) ; }}}


(defun top-line-title (body &key enabled) "check if body has a title line" ; {{{
	(let (frst frst-kind (enabled-rules (cons "h1" enabled)))
	(setq frst (nth 0 body))
	(setq frst-kind (formatting-result-kind (parse-line-kind frst nil :enabled enabled-rules)))
	
	;; discard empty lines
	(if (equal frst-kind "h1")  frst  nil)
)) ; }}}

(defun add-header-line (full-body) "add header line to body if it isn't there" ; {{{
	;; LATER: move this and top-line-title into row filters where it really belongs
	(let ((body (bop-page-body full-body)) text-title gen-title  ; body-lines
		(title (bop-page-title full-body)) (id (bop-page-id full-body))
		(bodyrules (bop-tablerow-bodyrules (bop-page-tablerow full-body)))
	)
	bodyrules  ; leave dummy alone
	
	;; check if body already has a title line
	(setq text-title (top-line-title body :enabled bodyrules))
	;; if it does not, check generated one
	(when (null text-title)
		(setq gen-title (generated-title title id))
	)
	
	;; only if the generated one is needed, add it
	(when (and (null text-title) (not (null gen-title)))
		(setq gen-title (join-str "# " (generated-title title id)))
		;; add title line to body
		(setf (bop-page-body full-body) (cons gen-title (cons "" body)))
		;; LATER: i considered auto-enabling h1 rule but you may not want that?
	)
	
	full-body
)) ; }}}

;;; }}}


;;; execute body of .lisp entry {{{

(defun run-entry (filename run-function) ; run entry file and get output {{{
	(let ((output (open-string)))
	
	(with-open-stream (*standard-output* (make-broadcast-stream))
	(with-output-to-string (s output)
		(load filename)
		(format s "~a" (funcall run-function))
	))
	
	(unless (null output)  (uiop:split-string output :separator (nn)))
)) ; }}}

(defun process-entry-body (render-function full-body is-reply) ; {{{
	(let (filetype
		(path (bop-page-path full-body)) ;(processed full-body)
	)

	(setq filetype (pathname-type path))
	
	;; if entry is a lisp file, replace entry body with its output
	(cond
	((equal filetype "lisp")
		(setf (bop-page-body full-body) (run-entry path 'test-main))
		
		;; BUG: there's not yet any good way to disable inserting breaks
		;; so just return the body without formatting
		(join-lines (bop-page-body full-body))
	)
	;; then render the body using the supplied function
	(t (funcall render-function  full-body is-reply)
	))
)) ; }}}

;;; }}}


;;; exporting to directory & web urls {{{

(defun export-page (str filename) "write a string to a file" ; {{{
	(with-open-file (strm filename :direction :output
		:if-exists :supersede :if-does-not-exist :create)
		(princ str strm)
)) ; }}}

(defun export-in-dir (dir filename body) ; {{{
	(let ((path
		(merge-pathnames dir filename)
	))
	(export-page body path)
)) ; }}}

(defun entry-web-url (entry  &key author filetype) ; {{{
;; currently only used for activitypub
;; LATER: combine this with exported-page-url? untested
	(let ((safe-id (format nil "~a" entry)) (tablerow (page-row entry)))
	
	;;(printl "WEB URL" entry (not-null tablerow))
	
	(cond
		((and (table:page-exists-p nil :row tablerow) (not-null author))
			(uri:construct-web-link
				:scheme 'https
				:host (uri:web-author-host author)
				:path (format nil (uri:web-author-path author) (uri:web-author-handle author))
				:name (page-basename tablerow) :type filetype))
		
		;; if link location starts with https:// etc, use it raw
		((has-protocol safe-id) safe-id)
		
		(t "#"))  ; if ENTRY is not clearly a web link or table row, return empty url
)) ; }}}

(defun exported-page-url (id filetype  &key filename) ; link to exported page {{{
"Look up entry data to create relative link to its html/gmi-exported webpage" 
	(let ((safe-id (format nil "~a" id)))
	
	(setq filename
	(cond
		;; webpages use the same name as the source file,
		;; so the html filename can simply be got to check if the page row exists
		((null filename)  (table:page-basename safe-id))
		;; filename can also be directly supplied
		(t (pathname-name filename))
	))
	
	(cond
		;; if page was found, construct its webpage filename
		((not (null filename))
			;; return "index.htm" etc for root
			(when (equal filename "root")  (setq filename "index"))
			(namestring (make-pathname
				:name (pathname-name filename)  :type filetype))
		)
		;; if link location starts with https:// etc, use it raw
		((has-protocol safe-id) safe-id)
		;; if link doesn't seem valid, return a blank link
		(t "#")
))) ; }}}

;;; }}}


;;; retrieve excerpt {{{

(defun excerpt-visible-outlinks (full-body) ; return outlinks provided by excerpt {{{
	(let (renderable
		(meta (bop-page-meta full-body))  (emptying-rel "empty")
	)
	
	(dolist (line meta)  ; for every meta line on excerpt
		(let (
			(rule (funcall (meta-line-kindsym line)))
			(rel (meta-line-rel line))
		)
		(when  ; collect meta line into 'renderable under one of these conditions
			(or
				(and  ; 1. if meta line is rendered AND not used to hide entry
					(line-rule-displayp rule)  (null (line-rule-deferp rule))
					)
				(equal rel emptying-rel)  ; 2. this line is for discarding meta section
			)
			(setq renderable (cons line renderable)))
	))
	
	(reverse renderable)  ; return renderable links ordered forwards
)) ; }}}

(defun excerpt-text (full-body) ; try to get page excerpt {{{
	(let ((meta-lines (bop-page-meta full-body))
		excerpt-id excerpt-page original-backlinks
		(excerpt-label "excerpt")
		;; LATER: allow user to define/choose what label(s) mark excerpts + emptying-rel
		;; that way the term can naturally localise to different languages.
	)
	
	;; search through meta lines to try to find first '@ ... excerpt' line
	(dolist (line meta-lines) ; {{{
		(let (
			(rule (funcall (meta-line-kindsym line)))
			(rel (meta-line-rel line)) (value (meta-line-value line))
		)
		
		;; if this is a back link such as cxt/"<=", save it to add to excerpt
		(when (line-rule-backlinkp rule)
			(setq original-backlinks (cons line original-backlinks)))
		;; when the first excerpt line is found, take it
		(when (and (null excerpt-id) (equal rel excerpt-label))
			(setq excerpt-id value))
	)) ; }}}
	
	;; attempt to load excerpt page if found
	(let (excerpt-raw meta renderable tablerow) ; {{{
		(unless (or
			(null excerpt-id)
			(null (setq excerpt-raw (table:page-body excerpt-id)))
			)
		
			(setf
				excerpt-page excerpt-raw
				(bop-page-excerpt excerpt-page) nil  ;; excerpt should not have an excerpt
				meta (bop-page-meta excerpt-page)
			)
			;; look through excerpt meta lines and keep only the usable ones
			(setq renderable (excerpt-visible-outlinks excerpt-page))
		)
		
		(unless (null excerpt-page)
			;; if none of excerpt's meta lines were usable, then...
			(cond
				((null renderable)  ; ...delete them all
					(setf (bop-page-meta excerpt-page) nil)
				)
				(t  ; ...or else use the usable ones
					(setf
						(bop-page-meta excerpt-page)  ; set meta section
							(append renderable (reverse original-backlinks))
				))
				;; note that if you use emptying-rel, the excerpt's meta section is not technically considered empty, so the backlinks are preserved.
			)
			
			(setf  ; correct excerpt's tablerow - basically just :threadlinks {{{
				tablerow (bop-page-tablerow excerpt-page)
				(bop-tablerow-threadlinks tablerow)  ; copy threadlinks from full entry
					(bop-page-threadlinks full-body)
				(bop-page-tablerow excerpt-page) tablerow
			) ; }}}
			
			;; run each table-filter again, mostly to correct outlinks field
			(setq excerpt-page (rules:run-table-filters excerpt-page))
		)
	) ; }}}
	
	excerpt-page
)) ; }}}

(defun will-excerpt-p (full-body  &key is-reply) ; {{{
	(let ((excerpt-limit *post-excerpt-limit*) body-len
		(body-lines (bop-page-body full-body)) (excerpt (bop-page-excerpt full-body))
	)
	
	(setq body-len (length (join-lines body-lines "")))
	
	(if (and
			(not-null is-reply)  ; entry MUST BE a reply
			(or
				(> body-len excerpt-limit)
				(not-null excerpt)))
			;; entry MAY BE either too long, or have manual excerpt attached
		t
		nil)
)) ; }}}

;;; }}}


;;; templating {{{

(defun reconstruct-meta-line (line) ; reconstruct individual meta line {{{
	(let (rule icon label
		(raw (meta-line-raw line))
		(value (format nil "~a" (meta-line-value line)))
	)
	
	(cond
		;; if not provided raw meta line text, attempt to reassemble meta line
		((null raw)
			(setq
				rule (funcall (meta-line-kindsym line))
				icon (line-rule-icon rule)
				label (meta-line-label line)
			)
			
			(when (null label) (setq label ""))
			(format nil "~a ~a ~a"  icon value label)
			
			;; LATER: this may skip over things like footnote codes
		)
		(t raw))
)) ; }}}

(defun reconstruct-raw-page (full-body) "spit out minimal version of raw page" ; {{{
	(let ((body (bop-page-body full-body)) (meta (bop-page-meta full-body)) raw output)
	
	;; body is stored as a list, so just throw it onto output (backwards)
	(setq output (reverse body))
	
	;; separate meta lines from body with a couple newlines
	(unless (null meta)
		(setq output
			(cons "" (cons "" output))
	))
	
	;; put meta lines onto raw page backwards
	(dolist (line meta)
		(setq
			raw (reconstruct-meta-line line)
			output (cons raw output))
		
		;; LATER: allow excluding comment lines
	)
	
	(join-lines (reverse output))
)) ; }}}

;;; }}}


;;; publish helpers {{{

(defun parse-wants (wants) ; {{{
	(let (target found-target non-revision (default-format *default-publish-format*))
	;; by default, use the default-publish-format set in init.lisp or bop-render
	
	;; check if 'wants' is a publish target, and if not create empty one  {{{
	(setq target (setq found-target (bop-render-targets:get-publish-target wants)))
	(unless (publish-target-p found-target)
		(setq target (make-publish-target :name nil :func default-format))
		(when (null found-target)
			(setq found-target target)
	))
	;; }}}
	
	(let ((pages (publish-target-pages found-target)))
	(cond
		;; LATER: make sure activitypub works again
		; ;; when PAGES is a string,
		; ;; assume publish function will do something special with page list
		; ((stringp (publish-target-dirs found-target)) '()  ; {{{
		;	(setq non-revision (publish-target-pages found-target))
		;	;; LATER: is there a better way to do extensions?
		;) ; }}}
		;; if given a publish target with pages, use them
		((not-null pages)  (setq non-revision pages))
		;; if 'wants' exists as a directory, get the pages in that directory
		;; LATER: this doesn't actually check the directory
		((stringp wants)
			(let ((dirs (targets:dir-pages wants)))
			(setq non-revision dirs)
		))
		;; if not passed any dir/target/etc as 'wants', get all non-revision pages
		(t  (setq non-revision (table:non-revision-pages)))
	))
	
	;; return publish target containing final set of pages
	(setf (publish-target-pages target) non-revision)
	target
)) ; }}}

;; currently unused
(defun export-announce (&key wants fmt-name output) ; {{{
	(let ((wants
		(if (null wants) "." wants)
	))
	(format t "Exporting ~a as ~a to ~a~%" wants fmt-name output)
)) ; }}}

;;; }}}


;;; "bop publish" command {{{

;; experimental
;; LATER: turn into a register filter thing like with meta lines
(defun render-filters (target) ; {{{
	;; perform tweaks on entries if requested
	(dolist (pagename (publish-target-pages target))
		(let ((row
			(page-row pagename)))
		
		;; if told to discard reply previews for a format like twinned, mark entries with no-replies rel
		;; BUG: this requires the bop-rules system and I can't figure out which system was supposed to depend on it, so during testing I just loaded it in init.lisp
		(unless (null *discard-previews*)
			(setf (bop-tablerow-outlinks row)
				(cons
					(make-meta-line
						:kindsym 'meta:rule-obj  ; "@"
						:value "pragma" :rel "no-replies")
						;; LATER: allow configuring no-replies string
					(bop-tablerow-outlinks row)))
			(put-page row))
	))
) ; }}}

(defun publish-bop (&key pwd wants action) ; publish wiki folder to html etc {{{
"render set of pages specified by WANTS to appropriate publish format in appropriate directory"
	(find-all-pages pwd)  ; get known pages
	(let (target format-func expander subset t-setup)
	action  ; leave dummy alone
	
	(setq
		target (parse-wants wants)
		format-func (publish-target-func target)
		t-setup (publish-target-setup target))
	
	;; if WANTS is null, assume all pages are needed, otherwise don't
	(unless (null wants)
		;; particularly complex publish targets might have their own :expander function to flatten them to one list of needed pages
		(setf
			expander (publish-target-expander target)
			subset
				(if (null expander)
					(publish-target-pages target)
					(funcall expander target))
		
			;; if subsetter finds more attached pages, add them to subset
			(publish-target-pages target) (table:subset-tables subset))
		)
	
	;; if provided, run publish target's function for preparation/settings
	(unless (null t-setup)  (funcall t-setup))
	
	(funcall format-func target pwd)
)) ; }}}

(register-command "publish" 'publish-bop)

;;; }}}
