;;; render.lisp - core for rendering things {{{
(defpackage :bop-render-targets
	(:use :common-lisp :bopwiki)
	(:local-nicknames (:table :bopwiki))
	(:export
		;; #::publish-target
		#:publish-target-name #:publish-target-dirs #:publish-target-pages #:publish-target-func #:publish-target-expander #:publish-target-setup  #:make-publish-target #:publish-target-p
		
		;; *target-table*
		#:register-publish-target #:get-publish-target
		
		#:dir-pages #:tag-pages
))
(in-package :bop-render-targets)
;;; }}}


;;; table for storing targets {{{

(defstruct publish-target ; {{{
	;; named publish target
	name  ; name to call target within 'bop publish'
	pages  ; exact pages
	func  ; function to publish with
	expander  ; function to convert :pages list to real pagenames, if necessary
	setup  ; function which sets settings for only this specific publish target
	
	;; note: 'publish-target isn't exported because I wanted to use it to create them in my shortcuts package 'bop-valenoern-shortcuts-render
) ; }}}

(defparameter *target-table* (make-hash-table :test 'equal))

;;; }}}


(defun register-publish-target (&key target  name data pages func expander setup) ; {{{
;; generally meant to be used in a stem folder's init.lisp
	(let ((flat-data nil))
	
	;; either target or fields can be defined; if target not defined use fields
	(when (null target)
		(setq target (make-publish-target :name name :pages pages :func func :expander expander :setup setup))
		)
	
	(unless (null data)
		;; interpret DATA argument, which can be several different things
		(dolist (item data)
			(cond
				;; if passed a list, flatten it to individual pages
				((listp item)
					(setq flat-data (append item flat-data)))
				;; by default add data item onto pages
				;; this avoids flattening special kinds of data
				(t  (setq flat-data (cons item flat-data)))
			))
		
		;; if :data is present, overwrite :pages with :data
		(setf (publish-target-pages target) flat-data))
	
	;; put target into table
	(puthash (publish-target-name target) target *target-table*)
)) ; }}}

(defun get-publish-target (name) ; {{{
	(gethash name *target-table*)
) ; }}}


;;; get pages helpers {{{

(defun dir-for-target (dir pwd) ; {{{
;; helper to expand directories
	(let ((as-dir (pathname-inside (surer-pathname pwd) dir))  in-dir dir-pages)
	
	(setq in-dir (table:pages-from-directory as-dir))
	(unless (null in-dir)
		(setq dir-pages  (table:non-revision-pages :subset in-dir))
	)
	dir-pages  ; return list of pages from directory
)) ; }}}

(defun dir-pages (dir) ; get pages from directory {{{
	(let ((top bopwiki:*top-directory*) pages)  ; get data directory set in main.lisp
	(find-all-pages top)  ; create page table if doesn't exist
	
	(setq pages (dir-for-target dir top))
	pages
)) ; }}}

(defun tag-pages (tag) ; get pages in tag {{{
	(let ((top bopwiki:*top-directory*) pages)  ; get data directory set in main.lisp
	(find-all-pages top)  ; create page table if doesn't exist
	
	(setq pages (pages-with-tag tag))
	pages
)) ; }}}

;;; }}}
