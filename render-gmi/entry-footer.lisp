(defpackage :bop-render-gemini-footer ; {{{
	(:use :common-lisp :bopwiki :bop-render-core :bop-render-targets
	)
	(:local-nicknames  (:table :bopwiki) (:render :bop-render-core)) ;; uiop
	(:export
		#:gmi-page-date  ; gmi-date-string
		;; meta-link-gmi gmi-meta-line
		#:gmi-meta-block
))
(in-package :bop-render-gemini-footer)
;; }}}


(defun gmi-date-string (date) ; {{{
	;; LATER: allow different configurable date formats
	(let (visual)
	
	;; get date values out of decode-universal-time
	(multiple-value-bind
		(sec minute hour day month year weekday dst-p tz)
		(decode-universal-time (unix-to-universal-time date))
		sec weekday dst-p tz  ; unused required arguments
		
		(setq visual (format nil
			"~2,'0d/~2,'0d/~d ~2,'0d:~2,'0d"
			day month year hour minute))
		;; there's not really an invisible 'datetime' in gemini
	)
	visual
)) ; }}}

(defun gmi-page-date (full-body) ; {{{
;; show date of entry if date exists
	(let ((row (bop-page-tablerow full-body)) date id placeholder date-string)
	(setq
		date (bop-tablerow-date row)
		id (bop-tablerow-idno row)
	)
	;; fill in title if id is 0
	(unless (> id 0)  (setq id (bop-tablerow-title row)))
	
	(setq
		placeholder (page-date-placeholder date)
		date-string
		(if (null placeholder)
			(gmi-date-string date)  placeholder)
	)
	
	(format nil "~a - ~a" date-string id)  ; n dash
	;; return string of date or placeholder
)) ; }}}

(defun meta-link-gmi (icon text) ; {{{
	;; get url of context link
	(let ((url (exported-page-url text "gmi")))
	
	(join-str
	"=> " url "  [" icon "] " text
	)
)) ; }}}

(defun gmi-meta-line (kind value) "render meta line to gemini" ; {{{
	(cond
	((equal kind "cxt")
		(meta-link-gmi "<=" value))
	((equal kind "fwd")
		(meta-link-gmi "=>" value))
	((equal kind "ver")
		(meta-link-gmi ">>" value))
	((equal kind "full")
		(meta-link-gmi "==" value))
	((equal kind "obj")
		(meta-link-gmi "@" value))
	(t '())
)) ; }}}

(defun gmi-meta-block (full-body) ; {{{
	(let (page-date obj-lines meta-block)
	page-date  ; leave alone
	
	;; take meta lines out of page object and flatten them into meta-block
	(let ((meta (bop-page-meta full-body)))
	;; for each meta line and what kind it is
	(dolist (line meta)
		(let ((kind (meta-line-kind line)) (value (meta-line-value line)) str)
		;; render meta line
		(setq str (gmi-meta-line kind value))
		
		;; if this is attachment, isolate to end of meta section, otherwise continue
		(cond
			((equal kind "obj")
				(setq obj-lines (cons str obj-lines)))
			((null str) '())
			(t (setq meta-block (cons str meta-block)))
		)
	)))
	
	(unless (null meta-block)
		(setq meta-block
			(join-lines (reverse meta-block))
	))
	
	meta-block  ; return meta-block
	
	;; BUG: seems not all meta lines are showing?
)) ; }}}
