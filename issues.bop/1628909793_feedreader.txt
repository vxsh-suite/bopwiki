'feedreader' extension (likely separate repo) to download feed entries as bop notes

* save youtube video citation cards to build private 'watch later list'

this kind of thing would be /way/ bigger in the era of a cross-platform mobile app where it actually became practical to 'sneakernet' your bop folders across devices. [1]
for now it'd simply be a desktop watch later list that's more durable over time as videos disappear


=> https://en.wikipedia.org/wiki/Sneakernet  1. sneakernet - any transfer method that requires no internet ;
<= 1628400212 deskbop
; <= 2397889969 later
# md-li
# md-li-x
