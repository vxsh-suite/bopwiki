* create basic 'print export' - an html? file that tests a print/epub bopwiki
  * entries should mostly be grouped into threads, but truly individual pages can be sorted together
  * table of contents by pure timestamp, table of contents by thread, then thread pages
  * context links to entry show entry's timestamp number and print page number
  * warnings for having external webpages in "=>" rather than an "==" copy, implying you don't have an excerpt / full citation inside your wiki folder and your link could become useless

<= 2397889969 later
; md-li-x
# md-li
