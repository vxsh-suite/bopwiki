# function lines [experimental]

as of ver 2022-1-03, bop has an experimental feature to configure settings from within entries.
to do this, you use a "()" line:

```
 () bop-rules-meta-define:assign-meta-line #> bop-rules-meta-nicefwd:rule-fwd-cite
 #> [1.] Example link title  | https://example.com
```

in this example, a new "#>" meta line is defined to offer an alternate "=>" syntax (this syntax is internally named 'rule-fwd-cite).

function lines and the command 'assign-meta-line are both very early, so this example is basically the only thing they can do right now.
later on, they are intended to be able to configure any number of stem settings you currently have to set in init.lisp and register any number of optional meta line types.


<= 1620354787 meta lines ;
; LATER: unbreak pre tags so it's not necessary to space-escape every rule
