# context links

Context links are a kind of meta line [*] that connect pages to other pages.

"<=" links are a basic, undifferentiated context link. By default, they cause the current entry to appear as a reply to their target, along with every level of its replies if they also use this link type.
(If you are building your own publish format, you can make your own decisions on the fine details of their behaviour.)

"=>" links are outgoing context links, intended to be appropriate for linking external webpages.
By default, they display their target as a reply to the entry but do not recurse into further replies.

"==" links represent the same content in an alternate location. For example, a current share link for an archived social media post, a multimedia item the entry is transcribing, or a long article the entry is summarising.
Whether to use this line type over "<="/"=>" can be subjective, but a good general rule is to use "==" if the source link is the exact same content and another line type if it is different.


<= 1620354787 meta
;<= 947217626 docs
