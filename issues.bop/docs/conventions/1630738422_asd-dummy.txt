# empty comments in .asd files

example:
```
(defsystem
	;; ... system definition ...
)
;
```

Sometimes when you change structure definitions in a lisp program, sbcl does not recompile them soon enough and complains that another file you're using expects the wrong structure definition.
I haven't found a better way to fix this than to toss a dummy comment line into the asdf system that contains the structs in question - or remove one that's already there - to make sbcl recompile the whole system.

These dummy comments are not important and meant to be removed.


<= 1630738412 conventions
# md-pre
; cr. 1630738412 / 1630738422 approx.
