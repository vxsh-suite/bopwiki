21-12-09: 'web-link and 'web-author

initially, the ActivityPub format introduced 'ap-user, with the assumption only ActivityPub objects would have to deal with the idea of an ActivityStreams "actor".

but it eventually turned out this was needed for rendering absolute urls period,
so in order for publish formats to generate absolute urls, 'ap-user and 'ap-link had to be moved into bop-render, breaking and revamping any ActivityPub-related functions that used them.

at this point, any format that will refer to rendered bop entries as if they're on an "external website" should use bop-render's 'web-link and 'web-author, but can create wrapper functions or objects around them to do things tailored to that format.


=> 1639011601 web-author issue
<= 1620355787 breaking changes
