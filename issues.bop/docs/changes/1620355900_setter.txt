21-05-05: init.lisp redefinitions

since this version redefining functions in init.lisp is being phased out in favour of using 'setter' functions to set configuration.
so far this affects:
* post-excerpt-limit , post-cutoff-limit - takes one arg to set value
* publish-bop-html - "

<= 1620355787 changes
