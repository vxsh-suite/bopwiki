# bop stems

The medium that bop helps you create is called a "bop stem". It may also be called a "data folder", "wiki folder"/"wiki", "microwiki", "blog", or other things to try to make the idea more intuitive to people who haven't heard of it before.

A "bop stem" consists of a bunch of small segments connected to each other in such a way that they put each other in context.
These segments - called "entries" or "pages" - could be small short ideas, long blog posts, or maybe some arbitrary data structure like a very precise book citation which will then be connected to notes on the book.

This is where "bop" gets its name - the image of a "stem" developing a little like a bamboo forest, with discrete segments forming off each other but also giving rise to new stalks horizontally. [1]

So far, I have mainly used bop stems to publish sets of interconnected microblog threads which then function a little like one giant editable microblog thread. [2]
But in theory, you could imagine other use cases where bop entries are visually rendered as a proper graph [3], and a "thread" of posts within some particular scope goes in all directions. This might be good for creating a visual sitemap of every general topic in a bop stem.


There are a few other characteristics to bop stems.

1. bop is a document-centric data format. You should be able to edit /all/ the raw data in a bop stem with /any/ text editor or basic unix-like command line, even if that might not be as efficient as something designed with bop stems in mind.

2. Different bop stems should be able to be merged into each other or separated without a lot of trouble, given that the boundaries between ideas are not always well-defined.

3. Entries in a bop stem are usually identified by creation date - for example, this entry was created on 2021-12-11 03:52 GMT, which becomes its ID number. This is a simple way to assign entries a "name" which is somewhat objective and somewhat permanent, and makes folders less necessary.


One of the major functions of the "bop" program, versus just editing entries by hand, is to separate the /physical organisation/ of files in a bop stem from the /entry data/ in the stem.
To the program, an entry is always just "12-11 03:52", so you cross-reference it in another entry by its date number.
Meanwhile you can give the entry a human-readable label in its filename like "stem", and a different nickname or note every time you cross-reference it which is usually ignored.

Thanks to this label, a human can somewhat easily distinguish entry files (the program can also find entries by labels in some cases), but changing it usually will not break anything. (URLs of published webpages are the main exception.)

And thanks to the date number, entries are already partially organised. A thread of entries posted on the same day will likely line up together in any file manager, although it may need its own "thread folder" or "general topic folder" if any completely different ideas were recorded in the same hour.
This way, the stem author is freed from having to think very hard about what "folder" small thoughts go into just to save or find them. They go next to whatever immediately led to them, or whatever single flat list of files you'd like to see them in. And either way it doesn't matter much; as long as you can find them using a thread or graph rendering you can "bop open" them for editing using their ID. (even cooler, whatever editor the entry file opens in will likely show you where you put it)


bop stems have a very "layered" philosophy.
I started by creating the first one manually without any special computer program at all; in fact, I tried to make the basic idea simple enough you could conceivably make a bop stem with pen and paper and a clock as a traditional "card box". [4]

But as the idea developed, I wanted to create a "card box" which would be both powerful enough to automatically transform the same data into many different mediums and uses, and simple to understand at some future date when whatever computer it was created on wasn't available and all you had was some kind of random hard disk or backup tape or printed book of bop entries.

As I add features to the program, I first try to think of the most intutitive way I can already do them with only the features bop already has. This inspires the way I will then simplify expressing that thing.

For example, I might start with the ability to add external links, but no way to label them or insert them at specific places in the text.
After all, you might be making a bop stem without any internet. You might find it's a lot less distracting to be able to write a whole blog post or story draft in a plain text editor without big long links interrupting the text you write as in markdown, TiddlyWiki, etc. You might find it's slightly better for your sleep to be able to close down your browser for the evening and write down ideas for a while without opening any internet-connected "App".

So, I have external links in the metadata section, then in my own blogs I start labelling them in plain text lines right before the metadata section,
then I might make it possible to add friendly titles to links from /within/ the metadata section so it's easier to tell which link the labels were supposed to go to,
then I might make the html renderer actually link footnote numbers or letters to the links.

(As of right now, those two features are planned but not implemented. I wish they were, they're probably fairly soon in the queue, but there are so many things to program and I can't do them all at once.)


;
=> https://en.wikipedia.org/wiki/BOP_clade  1. BOP clade ;
=> https://mastodon.help/#ThreadsAndRamifications  2. mastodon threads example - mastodon.help ;
=> https://en.wikipedia.org/wiki/Directed_graph  3. Directed graph - wikipedia ;
=> https://en.wikipedia.org/wiki/Zettelkasten  4. Zettelkasten - wikipedia ;
;
<= 1620354080 basics  n. basics ;
@ 4639194725 excerpt
; cr. 1639194725
