# Loading extensions

bop extensions are simply asdf systems [1,2] that build on existing bop code.
To load them, it's possible to use the standard method for most Common Lisp code, asdf:load-system.

```
;;;; init.lisp

(asdf:load-system "bop-render-publishgroup")
```

However, given bop is not yet to its "1.0" release where every important feature is stabilised, your lisp compiler can end up crashing and not running the program when you load extensions that don't exist - for example, if you wrote your init.lisp for a newer version of bop and moved to an older version without that extension. --(This happens very easily when developing bop.)--

For this reason, bop provides (load-extensions) to load present extensions and ignore missing ones.

```
;;;; init.lisp

(bop:load-extensions "bop-render-publishgroup" "bop-render-activitypub" "bogus-system")
;; the first two extensions will be loaded if installed; "bogus-system" will load nothing
```


=> https://common-lisp.net/project/asdf/#what_it_is  1. overview/explanation of asdf ;
=> https://common-lisp.net/project/asdf/asdf/The-defsystem-form.html  2. asdf manual - example of an asdf system and what goes in it ;
;
<= 1620354797 init.lisp
@ 4628912743 excerpt
; # md-pre
; FIX BUG: pre areas are bleeding through the whole entry
