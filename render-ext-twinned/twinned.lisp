;;; render-twinned.lisp - gamebook format experiment {{{
(defpackage :bop-render-twinned
	(:shadowing-import-from :bop-render-twinned-html
		#:colour-scheme-paper)
	(:shadowing-import-from :bop-render-twinned-rules
		#:rule-parser-choice #:rule-fork)
	(:use :common-lisp :bopwiki)
	(:local-nicknames
		(:table :bopwiki-core-table)
		(:meta :bop-rules-meta)
		
		(:render :bop-render-core) (:target :bop-render-targets)
		
		(:template :bop-render-html-template)
		(:footer :bop-render-html-footer)
		(:html :bop-render-html-2)
		
		(:ttemplate :bop-render-twinned-html)
	)
	(:export
		;; imported
		#:colour-scheme-paper
		#:rule-parser-choice #:rule-fork
		;; this package
		#:publish-bop-twinned
))
(in-package :bop-render-twinned)
;;; }}}


(defun publish-bop-twinned (target cwd) ; publish function {{{
	(let (output page-list)
	cwd  ; required but unused argument
	
	;; standard stuff from render html {{{
	;; LATER: this was revamped on another branch but it's not ready to be ported
	(when (null html:*publish-html-path*)
		(html:publish-html-path render:*default-publish-path*))
	(setq output (uiop:ensure-pathname html:*publish-html-path*))
	(ensure-directories-exist output)
	(setq page-list (target:publish-target-pages target))
	;; }}}
	
	;; temporarily swap out some of the default functions of render html
	;; BUG: currently this overrides init.lisp, and it should be the other way around
	(table:put-alternate-function 'template:html-template 'ttemplate:html-template-twinned)
	(table:put-alternate-function 'footer:html-meta-block 'ttemplate:choices-block-twinned)
	;; turn off rendering replies
	(render:discard-previews)
	
	;; export javascript for use on html pages
	;; LATER: I'd like to make it so you can choose to play these without a browser or javascript, and only run them in a lisp GUI. but this is simpler for the time being
	(render:export-in-dir  output (make-pathname :name "twinned" :type "js")  (ttemplate:html-script-twinned))
	
	;; defer to plain old render-html publish function
	;; this will publish to the html output path
	(html:publish-bop-html target cwd)
)) ; }}}
