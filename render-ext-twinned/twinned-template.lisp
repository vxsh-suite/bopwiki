;;; twinned-template.lisp - html templating functions
(defpackage :bop-render-twinned-html ; {{{
	(:use :common-lisp :bopwiki
		:bop-render-core
			;; rendered-entry-...
	)
	(:local-nicknames
		(:table :bopwiki-core-table)
		(:template :bop-render-html-template) (:style :bop-render-html-style)
		(:footer :bop-render-html-footer)
		(:render :bop-render-core)
		;; (:html-template) (:puri)
	)
	(:export
		#:meta-link-twinned
		#:choices-block-twinned
			;; note: 'CHOICES-BLOCK-TWINNED is reused as an arbitrary symbol to mark gamebook choices ("=>>"/fork) in :bop-render-twinned-rules
		#:html-template-twinned
		
		;; click-random-link-event  window-onload
		#:colour-scheme-paper
		#:html-script-twinned
))
(in-package :bop-render-twinned-html) ; }}}


;;; html {{{

(defun colour-scheme-paper () ; {{{
	;; very minimalist colour scheme designed for twinned
	(setf
		style:*html-body-colour* "#fbfcff"
		style:*html-entry-colour* "rgba(255, 255, 255, 0.73)"
		style:*html-border-colour* "#d0d2ea"
		)
	
	(table:put-alternate-function 'style:html-stylesheet-extra
		(lambda ()
"article { border-width: 2px; }

article .meta { margin-top: 2.2em; }
article footer { margin-top: 2.22em; }"
			))
	;; LATER: maybe just move twinned tweaks to a second stylesheet right after style.css
	
) ; }}}

; TODO: move to rules
(defun meta-line-randomlist (line) ; {{{
	(let (page-name row rel-func page-list)
	
	(cond
		((meta-line-p line)
			(setq
				page-name (meta-line-value line)
				row (page-row page-name)
				rel-func (meta-line-rel line)
				)
			;; if it makes any sense to call REL-FUNC on ROW, do
			(when (and (page-exists-p nil :row row) (not-null rel-func))
				(setq page-list (funcall rel-func row))
				;; try to flatten meta lines in list to simple page names
				(when (> (length page-list) 0)
					(mapcan  ; redo list with the following procedure:
						(lambda (item)
							(list (meta-line-value item))
							)
						page-list)))
		)
		(t nil))
)) ; }}}

(defun meta-link-twinned (icon title visual-label  ; meta link boilerplate {{{
	&key url label random-next)
	
	(let (dummy)
	dummy  ; leave alone
	
	(format nil
"		<b title=\"~a\" aria-hidden=\"true\">~a</b> <a href=\"~a\" aria-label=~W~A>~a</a>"
			title icon  url
			;; escape link labels with quotes to avoid aria-labels getting horribly busted
			(html-template:escape-string
				(format nil "~a: ~a" title label))  ; aria-label=~W
			
			;; "random:" link data - ~A
			(if (null random-next)
				""
				(format nil " data-random=\"~a\"" random-next))
			visual-label)
	
	;; LATER: be able to print static lists of 'random' page links in a <noscript>, and hide if javascript is available
)) ; }}}

(defun meta-line-twinned (line) ; {{{
	(let ((value (meta-line-value line))
		safe-value value-label visual-label icon caption outback url random-page-list
	)
	
	;; read rule information
	(let ((rule (funcall (meta-line-kindsym line))) randomlist) ; {{{
		(setq
			caption (line-rule-label rule)  ; friendly label, "context" etc
			outback (or (line-rule-outlinkp rule) (line-rule-backlinkp rule))
			;; escape possible "<"/">" in meta line icon
			icon  (html-template:escape-string (line-rule-icon rule))
			)
		
		;; if this is a gamebook choice
		(when (equal (meta-line-rel line) 'choices-block-twinned)
			;; try to interpret random: uri
			(setq randomlist (meta-line-randomlist value))
			;; if there was a list of pages, flatten it to a string
			(unless (null randomlist)
				(setq
					randomlist
					(mapcan  ; redo list with the following procedure:
						(lambda (entry)
							(list (render:exported-page-url entry "htm"))
							)
						randomlist)
					random-page-list (format nil "~a" randomlist))
				)
		)
		;; BUG: if a random link would accidentally point to the same page that is not removed
		;; but I don't feel like fixing that right now
		
	) ; }}}
	
	;; note: this was copied out of meta-link-html
	(let (
		(label (meta-line-label line))
		(numeric-id (bop-tablerow-idno (table:page-row safe-value))
		))
		
		;; if line includes a visual label use it, otherwise use :value
		(cond
			((null label)
				(setq value-label (format nil "~a" value))
				;; fill in empty labels with integer id, non-zerofilled
				(when (> numeric-id 0)
					(setq
						value-label numeric-id
						visual-label numeric-id))
			)
			(t
				(setq value-label label)
			)
		)
		(when (null visual-label)
			(setq visual-label value-label))
		
		(setq url (render:exported-page-url value "htm"))
	)
	
	(when (not-null outback)  ; if this is a typical outlink/backlink, stringify
		(meta-link-twinned  icon caption visual-label
			:url url  :label value-label  :random-next random-page-list))
	
	;; LATER: meta-line-twinned and meta-link-twinned both need to be cleaned up along with render-html, there is kind of an improper organisation of the steps inside that made it necessary to redundantly copy everything
)) ; }}}

(defun choices-block-twinned (full-body replyp parent) ; render all meta lines {{{
	(let (meta-lines hidden-lines  ; excerpt-label will-excerpt meta {{{
		(excerpt-label "excerpt") (emptying-rel "empty")
		(will-excerpt (will-excerpt-p full-body :is-reply replyp))
		(meta (bop-page-meta full-body))
	) ; }}}
	;; twinned doesn't have replies so don't hide parent links
	(setq parent nil)
	
	;; replace meta block with excerpt's meta block if applicable
	;; LATER: chop this out as it is the same in the default meta block function
	(let ((excerpt (bop-page-excerpt full-body)) meta)
		(unless (or
			(null will-excerpt) (null excerpt)  (null (setq meta (bop-page-meta excerpt)))
			)
			(setf (bop-page-meta full-body) meta)
	))
	
	;; for each meta line and what kind it is, collect or discard
	(dolist (line meta) ; {{{
		(let ((rel (meta-line-rel line)) canonical-value str)
		(setq canonical-value (table:page-id (meta-line-value line)))
		
		(unless (or  ; render meta line unless...
				;; - it's doing something special, like adding an excerpt or clearing the meta section
				(equal rel excerpt-label)
				(equal rel emptying-rel)
				(equal rel "no-replies")
			)
			(setq str (meta-line-twinned line))
			;; here this internally calls 'meta-link-twinned
		)
		
		(cond
			;; if line is scrapped or otherwise empty, discard
			((null str) '())
			;; if this line is a choice, put it in the choices list
			((equal rel 'choices-block-twinned)
				(setq meta-lines (cons str meta-lines)))
			;; otherwise, put it in the hidden choices
			(t  (setq hidden-lines (cons str hidden-lines)))
			)
	)) ; }}}
	
	;; stringify meta lines
	(unless (null meta-lines) ; {{{
		(format nil
"<div class=\"meta\">
~a~A
</div>"
			(template:html-meta-list (reverse meta-lines) :classname "choices")
			(if (null hidden-lines)
				""
				(format nil
"

	<template>
~a
	</template>"
					(template:html-meta-list (reverse hidden-lines) :classname "hidden"))
				)
	)) ; }}}
)) ; }}}

(defun html-template-twinned (title body) ; html page boilerplate {{{
"boilerplate for rendering into an html page"

	(format nil
"<!doctype html>
<html><head><meta charset=\"utf-8\" />
	<meta name=\"viewport\" content=\"initial-scale=1\">
	<title>~a</title>
	<link href=\"./style.css\" rel=\"stylesheet\" />
	<script src=\"./twinned.js\" defer></script>
</head><body>
<main>~a
</main>
</body></html>~%"
	title
	body
)) ; }}}

;;; }}}


;;; javascript {{{
;; I actually do not like javascript on web pages much,
;; but keep in mind this is based on the same general ideas as Twine, which generated html pages with javascript (which in my opinion was kind of bloated, and which I always ended up remaking from scratch)

;; fake function so I can get a little bit of folding and syntax highlighting in my editor
(defun click-random-link-event ()  ; clickRandomLink = function (e) {{{
;; actual regex: /^\((.*)\)$/g
"
	var target, entryString, selected, regex = /^\\((.*)\\)$/g;
	
	if (e.target instanceof Node) {
		entryString = e.target.getAttribute('data-random');
		if (entryString[0] == '(') {
			entryList = entryString.replaceAll(regex, '$1').split(' ')
		}
		
		selected = entryList[ getRandomArbitrary(0, entryList.length - 1) ];
		e.target.href = selected;
		
		console.log('wow, event listener', entryList, selected);
	}
"
) ; }}}

(defun window-onload () ; window.onload = function () {{{
"
	var choices = document.querySelectorAll('.meta a');
	// a[data-random]
	
	// I hate javascript sometimes
	if (choices instanceof Node) {
		choices = [ choices ];
	}
	for (i = 0; i < choices.length; i++) {
		choices[i].addEventListener('click', clickRandomLink);
	}
	
	console.log('CHOICES', typeof choices);
"
) ; }}}

(defun html-script-twinned () ; script to output to twinned.js {{{
(format nil
"
function getRandomArbitrary(min, max) {
	// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
	return Math.floor(Math.random() * (1 + max - min) + min);
}

var clickRandomLink = function (e) {~a};

window.onload = function () {~a};
"
	(click-random-link-event)
	(window-onload)
	)
) ; }}}

;;; }}}
