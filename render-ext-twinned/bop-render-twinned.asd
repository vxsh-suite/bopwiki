(defsystem "bop-render-twinned"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "bopwiki gamebook extension"
	
	:depends-on (
		"uiop" "cl-ppcre"
		"bopwiki" "bop-render"
		"bop-render-html"
	)
	:components (
		(:file "twinned-template")  ; :bop-render-twinned-html
		(:file "twinned-rules")  ; :bop-render-twinned-rules
		(:file "twinned" :depends-on ("twinned-template" "twinned-rules"))  ; :bop-render-twinned
))
