;;; publish-ytp.lisp {{{
(defpackage :bop-render-ytp-stylesheet-core  ; not the 'real' package. see bottom
	(:use :common-lisp :bopwiki)
	(:local-nicknames
		(:render :bop-render)
		(:targets :bop-render-targets)
		(:ytpo :ytp)  ; ytp stylesheet generator base code
		(:ytpv :ytp-stylesheet-videos)  ; ytp video-specific url abbreviator
	)
	(:export
		#:*publish-css-path* #:publish-css-path
		;; lines-from-pagename
		#:publish-bop-css
))
(in-package :bop-render-ytp-stylesheet-core)

;; configuration - publish-css-path {{{

(defparameter *publish-css-path* nil)

(defun publish-css-path (path)
	(let ((path-object (uiop:ensure-pathname path)))
	(setq *publish-css-path* path-object)
	;; BUG: just call a generic function in render for all formats
))

;; }}}

;;; }}}


(defun lines-from-pagename (pagename) ; load url list from bop entry {{{
	(let ((page (page-body pagename)) urls)
	
	(setq urls
		(if (null page)
			(list "")
			(bop-page-body page)))
	
	urls
)) ; }}}

(defun publish-bop-css (target cwd) ; export multiple lists to one stylesheet {{{
	(let (output first-entry (result (open-string))
		(style-list (targets:publish-target-pages target))
		(entry-cache (make-hash-table :test 'equal))
	)
	cwd  ; unused required argument
	
	;; if no publish path, use the bop-render one
	(when (null *publish-css-path*)
		(publish-css-path render:*default-publish-path*))
	
	;; create directory, & export to user-configured directory
	(setq output (uiop:ensure-pathname *publish-css-path*))
	(ensure-directories-exist output)
	
	
	(with-output-to-string (s result)
	(dolist (style style-list)
		(let (css cache urls
			;; pagename for entry urls are stored in
			(comment (ytpo:style-rule-comment style))
			;; function to shorten urls
			(shortener  ; BUG: pass in desired function
				(function ytpv:shorten-url))
		)
		;; save name of first entry
		(when (null first-entry)
			(setq first-entry comment))
		
		;; check if url list has already been got,
		;; if so reuse it
		(setq cache (gethash comment entry-cache))
		(cond
			((null cache)
				(setq urls (lines-from-pagename comment))
				(puthash comment urls entry-cache)
			)
			(t (setq urls cache)))
		
		;; export css using ytp
		(setq css
			(ytpo:style-from-arg
				:shorten-url shortener
				:separator (format nil "")  ; separator between individual selectors
				:selector-list (list style)
				:url-list urls))
		
		(format s "~%~a" css)
	)))
	
	;; as long as there is a reasonable filename, export stylesheet
	(unless (null first-entry)
		(render:export-in-dir
			output (make-pathname :name first-entry :type "css")  result))
)) ; }}}


;; LATER: make sure this supports 'mastodon toot marker' stylesheet

;; export actual package
;; the compiler got very upset when I tried to do all of this in one package at the top
(uiop:define-package :bop-render-ytp-stylesheet
	(:use-reexport
		:ytp :ytp-stylesheet-videos :bop-render-ytp-stylesheet-core))
