(defsystem "bop-render-ytp"
	:author "Valenoern"
;	:version "0.0.1"
	:licence "GPL"
	:description "bopwiki ytp stylesheet generator integration"
	
	:depends-on ("uiop" "bopwiki" "bop-render" "ytp" "ytp-examples")
	:components (
		(:file "publish-ytp")  ; :bop-render-ytp-stylesheet
))
