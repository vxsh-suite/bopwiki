#!/bin/sh

# script to check if .sbclrc is usable

sbclrc=~/.sbclrc
addedlines=`echo "\n(require :asdf)\n\n(setf asdf:*central-registry*\n\t(list* '*default-pathname-defaults*\n\t\t#p\"/usr/share/common-lisp/systems/\" #p\"/usr/share/common-lisp/source/\"\n\t\tasdf:*central-registry*\n))"`
error=""

main() {
 echo "Checking ${sbclrc}..."

if [ -e "$sbclrc" ]; then
 closecheck
else
 echo "$sbclrc not found. Attempting to create one"
 echo "$addedlines" > "$sbclrc"
fi

}

closecheck() {
 asdf=`grep "require :asdf" $sbclrc`
 if [ -z "$asdf" ]; then
  echo "$sbclrc doesn't appear to (require :asdf)."
  error="y"
 fi

 systems1=`grep "#p\"/usr/share/common-lisp/systems/\"" $sbclrc`
 if [ -z "$systems1" ]; then
  systems2=`grep "#p\"/usr/share/common-lisp/source/\"" $sbclrc`
  if [ -z "$systems2" ]; then
   echo "$sbclrc doesn't appear to include /usr/share lisp directories."
   error="y"
  fi
 fi

 if [ -z "$error" ]; then
  echo "$sbclrc looks ok"
 else
  echo "You may need to fix your ~/.sbclrc."
  echo "An example working ~/.sbclrc looks like this:"
  echo "$addedlines"
 fi
}

main
