#!/bin/sh

# try to determine which kind of package manager installer is installing for
# simply call "distro.sh", to output either the distro name or "none"

main() {
 distro=""

 # test for each package manager with has_program
 program="pacman"; has_pacman=`has_program`
 program="apt"; has_apt=`has_program`

 if [ -n "$has_pacman" ]; then
  distro="arch"
 elif [ -n "$has_apt" ]; then
  distro="debian"
 else
  distro="none"
 fi
 # echo result to stdout
 echo "$distro"
}

# print a string if package manager command exists, or nothing
has_program() {
 # print path of program if found
 # '2' suppresses output when program not found
 has_program=`which $program 2>/dev/null`
 # print string if path is not empty
 if [ -e "$has_program" ];
  then has_program="PRESENT"
  else has_program=""
 fi
 # print output as if this were a 'real' shell command
 echo "$has_program"
}

main
