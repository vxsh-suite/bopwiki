(defsystem "bop-rules"
;	:version "0.0.1"
	:description "bop meta line rules"
	:author "Valenoern"
	:licence "GPL"
	
	:depends-on (
		"cl-ppcre"  ; required for cite fwd
		"bopwiki"
	)
	:components (
		(:file "meta-basic")  ; :bop-rules-meta
		(:file "meta-define")  ; :bop-rules-meta-define
		(:file "meta-nicefwd" :depends-on ("meta-basic"))  ; :bop-rules-meta-nicefwd
))
