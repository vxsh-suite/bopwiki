;;; meta-define.lisp - tools to assign meta line icons to their effects {{{
(defpackage :bop-rules-meta-define
	(:use :common-lisp :bopwiki)
	(:local-nicknames
		(:parse :bopwiki-linerules-parse)
		(:rules :bopwiki-linerules)
	)
	(:export
		#:assign-meta-line
		#:rule-parser-func
		#:rule-func
))
(in-package :bop-rules-meta-define)
;;; }}}


(defun rule-parser-symbol (string-arg) ; parse string to symbol {{{
"Parse the supplied string STRING-ARG into a symbol.
if the symbol does not start with a package, it will be interned in :bopwiki-init"
	(let ((default-pkg "BOPWIKI-INIT"))  ;symbol-arg
	
	(multiple-value-bind (pkg-str name-str)
		(parse:split-value-at string-arg #\:)
		
		;; return symbol
		(if (null name-str)
			;; if symbol is unqualified, use a "likely" package such as :bopwiki-init
			(intern (string-upcase string-arg) default-pkg)
			;; if symbol is in PACKAGE:NAME fomat, use PACKAGE
			(intern (string-upcase name-str) (string-upcase pkg-str))
	))
)) ; }}}

(defun rule-parser-func (line) ; get arguments from func line {{{
	(let (
		;; split value at spaces
		(args
			(uiop:split-string (meta-line-value line) :separator " "))
	)
	
	;; return name of function to call in :rel
	(setf (meta-line-rel line)
		(rule-parser-symbol (first args)))
	
	;; return arguments after function name as :value ;
	;; arguments will be passed on as strings and not parsed
	(setf (meta-line-value line) (subseq args 1))
	
	line
)) ; }}}

(defun entry-filter-funcs (full-body) ; run each func line in entry {{{
	(let ((idno (bop-page-id full-body)) metas)
	
	;; try to only run filter while table is first being populated,
	;; and thus each page 'doesn't exist' yet
	(unless (page-exists-p idno)
		(setq metas (bop-page-meta full-body))
		
		;; look through meta lines for :commandp t
		(dolist (meta metas)
			(let (meta-parsed func-sym func-args
				(rule (funcall (meta-line-kindsym meta)))
			)
			(when (line-rule-commandp rule)
				(setq
					meta-parsed (rule-parser-func meta)
					func-sym (meta-line-rel meta-parsed)
					func-args (meta-line-value meta-parsed)
				)
				;; LATER: check what happens if function doesn't exist
				(funcall func-sym func-args)
			)
	)))
	
	full-body
)) ; }}}

(defun assign-meta-line (args  &key entry) ; tie icon to rule - "()"/func {{{
	(let (
		(icon (first args))
		(rule-symbol (rule-parser-symbol (second args)))
	)
	entry  ; dummy
	;; LATER: allow definitions to apply to a particular set of entries - issues.bop/1640582096
	
	(rules:put-rule rule-symbol  :icon icon)
	
	t
)) ; }}}


(defun rule-func () ; {{{
	(make-line-rule :sphere "meta"  :icon "()" :name "func" :label "call function"
		:commandp t  ; meta line's :rel will be called on :value
		:parser 'rule-parser-func
)) ; }}}


(rules:register-line-rule 'rule-func)
(rules:register-table-filter 'entry-filter-funcs)
