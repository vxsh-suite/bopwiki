;;; meta-basic.lisp - semi-core meta line rules {{{
(defpackage :bop-rules-meta
	(:import-from :bopwiki-linerules #:make-line-rule #:register-line-rules)
	(:use :common-lisp :bopwiki)
	(:local-nicknames 
		(:parse :bopwiki-linerules-parse))
	(:export
		#:rule-fwd #:rule-full #:rule-obj #:rule-tag #:rule-ver #:rule-time #:rule-cat
))
(in-package :bop-rules-meta)
;;; }}}


(defun rule-fwd () ; {{{
	(make-line-rule :sphere "meta"  :icon "=>" :name "fwd" :label "additional link"
		:displayp t :outlinkp t
		:parser 'parse:rule-parser-label-comment
)) ; }}}

(defun rule-full () ; {{{
	(make-line-rule :sphere "meta"  :icon "==" :name "full" :label "live version"
		:displayp t :outlinkp nil :backlinkp nil
		:parser 'parse:rule-parser-label-comment
		;; :aliasp t  - needs testing but should probably work this way
)) ; }}}

(defun rule-obj () ; {{{
	(make-line-rule :sphere "meta"  :icon "@" :name "obj" :label "media attachment"
		:displayp nil  ; don't display in normal list of outlinks
		:outlinkp t  ; available in :outlinks field
		:parser 'parse:rule-parser-comment-role
)) ; }}}

(defun rule-tag () ; {{{
	(make-line-rule :sphere "meta"  :icon "#" :name "tag" :label "post tag(s)"
		:displayp nil
)) ; }}}

(defun rule-ver () ; {{{
	(make-line-rule :sphere "meta"  :icon ">>" :name "ver" :label "revision of"
		:displayp t :backlinkp t
		:deferp t  ; hide entry with this line from main entries
		:parser 'parse:rule-parser-label
)) ; }}}

(defun rule-time () ; {{{
	(make-line-rule :sphere "meta"  :icon "::" :name "time" :label "post date"
		:displayp nil
		:aliasp t  ; alias value as name of this entry if not already taken
		:parser 'parse:rule-parser-role-comment
		;; LATER: this line is supposed to accept multiple comma-separated values, but that's not ready yet
)) ; }}}

(defun rule-cat () ; currently unimplemented {{{
	(make-line-rule :sphere "meta"  :icon "<>" :name "cat" :label "general context"
		:parser 'parse:rule-parser-label
		;; LATER: this line is in the middle of being fully implemented on another branch, but other things had to be changed
)) ; }}}


(register-line-rules (list 'rule-fwd 'rule-full 'rule-obj 'rule-tag 'rule-ver 'rule-time))
;; 'rule-cat
